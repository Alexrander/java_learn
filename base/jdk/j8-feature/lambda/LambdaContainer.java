package java8.lambda;

import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;

public class LambdaContainer {


    public static void main(String[] args){

        Supplier<Integer> no_parmas = ()-> 3;
        Function<Integer,Integer> single_param = m -> m++ ;
        BiFunction<Integer,Integer,Integer> two_param = (m,n) ->m+n;


    }

}
