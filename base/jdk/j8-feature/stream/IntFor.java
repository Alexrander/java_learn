package java8.stream;

import java.util.stream.IntStream;

public class IntFor {

    public static void main(String[] args){
        IntStream.range(1,5).forEach(System.out::println);
    }
}
