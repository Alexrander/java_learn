package 内存;

import sun.misc.Unsafe;
import sun.reflect.CallerSensitive;
import sun.reflect.Reflection;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class Test {

    public static Unsafe getUnsafeInstance() throws Exception
    {
        // 通过反射获取rt.jar下的Unsafe类
        Field theUnsafeInstance = Unsafe.class.getDeclaredField("theUnsafe");
        theUnsafeInstance.setAccessible(true);
        // return (Unsafe) theUnsafeInstance.get(null);是等价的
        return (Unsafe) theUnsafeInstance.get(Unsafe.class);
    }



    public static void main(String[] args) throws Exception {
//        int 数组大小间隔

        System.err.println(getUnsafeInstance().arrayBaseOffset(int[].class));
        System.err.println(getUnsafeInstance().arrayBaseOffset(long[].class));
        System.err.println(getUnsafeInstance().arrayBaseOffset(Object[].class));


    }
}
