public class ReferenceTricks {
    public int i;
//https://blog.csdn.net/stridebin/article/details/73639378
    public static void main(String[] args) {
        ReferenceTricks r = new ReferenceTricks();
         r.i = 0;

         System.out.println("Before changeInteger:" + r.i);
         changeInteger(r);
         System.out.println("After changeInteger:" + r.i);

        // just for format
         System.out.println();

        // reset integer
         r.i = 0;
         System.out.println("Before changeReference:" + r.i);

//         块状域 对引用有效  函数传递参数的秘密
//        　我刚才说什么来着？将变量复制一份到栈中，没错，“复制”！
         changeReference(r);
         System.out.println("After changeReference:" + r.i);
    }


    private static void changeReference(ReferenceTricks r) {
        r = new ReferenceTricks();
        r.i = 5;
        System.out.println("In changeReference: " + r.i);

    }



    private static void changeInteger(ReferenceTricks r) {
        r.i = 5;
        System.out.println("In changeInteger:" + r.i);
    }


}