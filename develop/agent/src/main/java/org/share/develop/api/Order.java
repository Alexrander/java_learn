package org.share.develop.api;

import java.io.Serializable;

public class Order implements Serializable {

    private Long id;
    private String name;
    private String location;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public static Order getInstance(){
        return  new Order();
    }

    public String toString(){
        System.err.println(1);
        return "123";
    }
}

