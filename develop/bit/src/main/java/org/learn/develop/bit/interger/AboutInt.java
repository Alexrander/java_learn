package org.learn.develop.bit.interger;

public class AboutInt {

    public static void main(String[] args){
        /********************************************************************************************************************
         *              numberOfLeadingZeros
         *              无符号前置0个数  负数为0     0 为32
        ********************************************************************************************************************/
        int a = -1;
        int b = 0 ;
        int c = 1;
        System.err.println(Integer.numberOfLeadingZeros(a));
        System.err.println(Integer.numberOfLeadingZeros(b));
        System.err.println(Integer.numberOfLeadingZeros(c));

    }
}
