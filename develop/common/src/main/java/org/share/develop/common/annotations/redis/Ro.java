package org.share.develop.common.annotations.redis;

import java.lang.annotation.*;

/**
 * 
 * 类注解  存取的prefix
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
public @interface Ro {
	
	String prefix();
	
}
