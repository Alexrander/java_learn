package org.share.develop.nio;

import java.io.FileNotFoundException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;

public class ChannelFactoy {

    public static FileChannel createFileChannel(String path) throws FileNotFoundException {
        return createFileChannel(path, "rw");
    }
    public static FileChannel createFileChannel(String path, String rw) throws FileNotFoundException {
        RandomAccessFile aFile = new RandomAccessFile(path, rw);
        return aFile.getChannel();
    }
}
