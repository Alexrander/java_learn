/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.share.develop.proxy.javassist;


import javassist.CtClass;
import org.learn.develop.utils.StringUtils;
import org.share.develop.proxy.compiler.AbstractCompiler;
import org.share.develop.proxy.javassist.support.CtClassBuilder;


import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class JavassistCompiler extends AbstractCompiler {


    private static final Pattern IMPORT_PATTERN = Pattern.compile("import\\s+([\\w\\.\\*]+);\n");

    private static final Pattern EXTENDS_PATTERN = Pattern.compile("\\s+extends\\s+([\\w\\.]+)[^\\{]*\\{\n");

    private static final Pattern IMPLEMENTS_PATTERN = Pattern.compile("\\s+implements\\s+([\\w\\.]+)\\s*\\{\n");

    private static final Pattern METHODS_PATTERN = Pattern.compile("\n(private|public|protected)\\s+");

    private static final Pattern FIELD_PATTERN = Pattern.compile("[^\n]+=[^\n]+;");

    @Override
    public Class<?> doCompile(String name, String source) throws Throwable {
        CtClassBuilder builder = new CtClassBuilder();
        builder.setClassName(name);

        // process imported classes
        Matcher matcher = IMPORT_PATTERN.matcher(source);
        while (matcher.find()) {
            builder.addImports(matcher.group(1).trim());
        }
        
        // process extended super class
        matcher = EXTENDS_PATTERN.matcher(source);
        if (matcher.find()) {
            builder.setSuperClassName(matcher.group(1).trim());
        }
        
        // process implemented interfaces
        matcher = IMPLEMENTS_PATTERN.matcher(source);
        if (matcher.find()) {
            String[] ifaces = matcher.group(1).trim().split("\\,");
            Arrays.stream(ifaces).forEach(i -> builder.addInterface(i.trim()));
        }
        
        // process constructors, fields, methods
        String body = source.substring(source.indexOf('{') + 1, source.length() - 1);
        String[] methods = METHODS_PATTERN.split(body);
        System.err.println(methods.length);
        System.err.println(methods[0]);

        String className = StringUtils.splitAndGet(name,"\\.",-1);

        Arrays.stream(methods).map(String::trim).filter(m -> !m.isEmpty()).forEach(method-> {
            if (method.startsWith(className)) {
                builder.addConstructor("public " + method);
            } else if (FIELD_PATTERN.matcher(method).matches()) {
                builder.addField("private " + method);
            } else {
                builder.addMethod("public " + method);
            }
        });
        
        // compile
        ClassLoader classLoader = getClass().getClassLoader();
        CtClass cls = builder.build(classLoader);
        return cls.toClass(classLoader, JavassistCompiler.class.getProtectionDomain());
    }


    public static void main(String[] args) throws Throwable {
        String  cls = "package org.share.develop.api;\n" +
                "\n" +
                "import java.io.Serializable;\n" +
                "\n" +
                "public class Order implements Serializable {\n" +
                "\n" +
                "    private Long id;\n" +
                "    private String name;\n" +
                "    private String location;\n" +
                "\n" +
                "\n" +
                "    public Long getId() {\n" +
                "        return id;\n" +
                "    }\n" +
                "\n" +
                "    public void setId(Long id) {\n" +
                "        this.id = id;\n" +
                "    }\n" +
                "\n" +
                "    public String getName() {\n" +
                "        return name;\n" +
                "    }\n" +
                "\n" +
                "    public void setName(String name) {\n" +
                "        this.name = name;\n" +
                "    }\n" +
                "\n" +
                "    public String getLocation() {\n" +
                "        return location;\n" +
                "    }\n" +
                "\n" +
                "    public void setLocation(String location) {\n" +
                "        this.location = location;\n" +
                "    }\n" +
                "\n" +
                "    public static Order getInstance(){\n" +
                "        return  new Order();\n" +
                "    }\n" +
                "\n" +
                "    public String toString(){\n" +
                "        System.err.println(2);\n" +
                "        return \"123\";\n" +
                "    }\n" +
                "}\n" +
                "\n";
//        String className = "com.test.Eval";
//        StringBuilder sb = new StringBuilder();
//        sb.append("package com.test;");
//        sb.append("public class Eval{public String toString(){System.err.println(123); return \"fef\";}}");
        JavassistCompiler javassistCompiler = new JavassistCompiler();
//        System.err.println(javassistCompiler.doCompile(className,sb.toString()).newInstance().toString());
        System.err.println(javassistCompiler.doCompile("org.share.develop.api.Order",cls).newInstance().toString());
    }
}
