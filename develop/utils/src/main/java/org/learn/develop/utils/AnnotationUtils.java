package org.learn.develop.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class AnnotationUtils {


    public static boolean existsAnnotation(Class<? extends Annotation> annotationClass, Class<?> classes){
        return existsAnnotation(annotationClass,new Class[]{classes});
    }
    public static boolean existsAnnotation(Class<? extends Annotation> annotationClass, Class<?>[] classes){
        if(classes != null && classes.length > 0){
            for (Class clazz : classes){
                if(clazz == null){
                    continue;
                }
                Method[] methods = clazz.getMethods();
                for (Method method : methods) {
                    if (method.getAnnotation(annotationClass) != null) {
                        return true;
                    }
                }
            }
        }
        return false;

    }
}
