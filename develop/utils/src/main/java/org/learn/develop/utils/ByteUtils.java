package org.learn.develop.utils;

import java.nio.ByteBuffer;

public class ByteUtils {

    public static int int_length = 4;

    public static byte[] intToByteArray(int intValue) {
        return ByteBuffer.allocate(int_length).putInt(intValue).array();
    }


    public static String bytesToHexString(byte[] src) {
        StringBuilder builder = new StringBuilder();
        if (src == null || src.length <= 0) {
            return null;
        }
        String hv;
        for (int i = 0; i < src.length; i++) {
            hv = Integer.toHexString(src[i] & 0xFF).toUpperCase();
            if (hv.length() < 2) {
                builder.append(0);
            }
            builder.append(hv);
        }
        return builder.toString();
    }

}
