package org.learn.develop.utils;

import java.util.Collection;

public class CollectionUtils {

    public static boolean isNotEmpty(Collection col){
        return col != null && col.size() > 0;
    }

    public static boolean isEmpty(Collection col){
        return !isNotEmpty(col);
    }
}
