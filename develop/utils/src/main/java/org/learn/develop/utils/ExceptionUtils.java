package org.learn.develop.utils;

import java.util.ArrayList;
import java.util.List;

public class ExceptionUtils {
    class Os{
        int a = 0;

        public int getA() {
            return a;
        }
    }


    void call(){
        try {
            inner();
        }catch (Exception e){
            e.printStackTrace();
            System.err.println(123);
        }

    }

    void inner(){
        List<Os> ab = new ArrayList<>();
        ab.add(null);
        System.err.println(ab.get(0).getA());
    }


    public static void main(String[] args){
        new ExceptionUtils().call();
    }
}
