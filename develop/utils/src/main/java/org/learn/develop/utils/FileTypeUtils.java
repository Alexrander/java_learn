package org.learn.develop.utils;


import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

public class FileTypeUtils {
    // 缓存文件头信息-文件头信息
    public static final HashMap<String, String> mFileTypes = new HashMap<String, String>();
    static {
        // images
        mFileTypes.put("FFD8FF", "jpg");
        mFileTypes.put("89504E47", "png");
        mFileTypes.put("47494638", "gif");
        mFileTypes.put("49492A00", "tif");
        mFileTypes.put("424D", "bmp");
        //
        mFileTypes.put("41433130", "dwg"); // CAD
        mFileTypes.put("38425053", "psd");
        mFileTypes.put("7B5C727466", "rtf"); // 日记本
        mFileTypes.put("3C3F786D6C", "xml");
        mFileTypes.put("68746D6C3E", "html");
        mFileTypes.put("44656C69766572792D646174653A", "eml"); // 邮件
        mFileTypes.put("D0CF11E0", "doc");
        mFileTypes.put("D0CF11E0", "xls");//excel2003版本文件
        mFileTypes.put("5374616E64617264204A", "mdb");
        mFileTypes.put("252150532D41646F6265", "ps");
        mFileTypes.put("255044462D312E", "pdf");
        mFileTypes.put("504B0304", "docx");
        mFileTypes.put("504B0304", "xlsx");//excel2007以上版本文件
        mFileTypes.put("52617221", "rar");
        mFileTypes.put("57415645", "wav");
        mFileTypes.put("41564920", "avi");
        mFileTypes.put("2E524D46", "rm");
        mFileTypes.put("000001BA", "mpg");
        mFileTypes.put("000001B3", "mpg");
        mFileTypes.put("6D6F6F76", "mov");
        mFileTypes.put("3026B2758E66CF11", "asf");
        mFileTypes.put("4D546864", "mid");
        mFileTypes.put("1F8B08", "gz");
    }





    public static boolean isExcel(InputStream inputStream){
        byte[] b = new byte[4];
        try {
            inputStream.read(b, 0, b.length);
            return mFileTypes.get(ByteUtils.bytesToHexString(b)).equals("xlsx")
                    || mFileTypes.get(ByteUtils.bytesToHexString(b)).equals("xls");
        }catch (Exception e){
            return false;
        }


    }


    public static void main(String[] args) throws IOException {
        String oss ="https://1919-new-bbc-test.oss-cn-beijing.aliyuncs.com/0e44b829-d565-42bb-86c3-2047655078c2";
        InputStream inputStream = IOUtils.readFrom(oss);
        byte[] b = new byte[4];
        inputStream.read(b, 0, b.length);
        System.err.println(mFileTypes.get(ByteUtils.bytesToHexString(b)));



    }

}
