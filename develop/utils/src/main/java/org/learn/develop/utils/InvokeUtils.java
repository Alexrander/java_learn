package org.learn.develop.utils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class InvokeUtils {

    public static Object invoke(Object object,String methodName,Object... args) throws NoSuchMethodException,
            InvocationTargetException, IllegalAccessException {
        Method method = object.getClass().getMethod(methodName);
        return method.invoke(object,args);
    }

}
