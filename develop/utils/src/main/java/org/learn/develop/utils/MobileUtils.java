package org.learn.develop.utils;

public class MobileUtils {
    private MobileUtils(){}
    public static String hidden(String mobile){
        return mobile.substring(0, 3) + "****" + mobile.substring(8);
    }
}
