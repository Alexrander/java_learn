package org.learn.develop.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NumberUtils {


    private static boolean isMatch(String regex, String orginal){
        if(orginal==null||orginal.trim().equals("")){
            return  false;
        }
        Pattern pattern = Pattern.compile(regex);
        Matcher isNum = pattern.matcher(orginal);
        return isNum.matches();
    }


    public static boolean isPositiveInteger(String orginal) {
        return isMatch("^\\+{0,1}[1-9]\\d*", orginal);
    }



    public static void main(String[] args){

    }
}
