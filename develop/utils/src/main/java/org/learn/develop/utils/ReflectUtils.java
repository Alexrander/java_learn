package org.learn.develop.utils;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public  class ReflectUtils {
    private ReflectUtils() { }
    private static final Pattern GETTER_PATTERN = Pattern.compile("(get|is)[A-Z].+");

    /**
     * Get the bean-style property names for the specified object.
     *
     * @param targetClass the target object
     * @return a set of property names
     */
    public static Set<String> getPropertyNames(final Class<?> targetClass)
    {
        HashSet<String> set = new HashSet<>();
        Matcher matcher = GETTER_PATTERN.matcher("");
        for (Method method : targetClass.getMethods()) {
            String name = method.getName();
            if (method.getParameterTypes().length == 0 && matcher.reset(name).matches()) {
                name = name.replaceFirst("(get|is)", "");
                try {
                    if (targetClass.getMethod("set" + name, method.getReturnType()) != null) {
                        name = Character.toLowerCase(name.charAt(0)) + name.substring(1);
                        set.add(name);
                    }
                }
                catch (Exception e) {
                    // fall thru (continue)
                }
            }
        }

        return set;
    }


    /**
     * 当卡青年类是否是基本类
     * @param cls
     * @return
     */
    public static boolean isPrimitives(Class<?> cls) {
        if (cls.isArray()) {
            return isPrimitive(cls.getComponentType());
        }
        return isPrimitive(cls);
    }

    public static boolean isPrimitive(Class<?> cls) {
        return cls.isPrimitive() || cls == String.class || cls == Boolean.class || cls == Character.class
                || Number.class.isAssignableFrom(cls) || Date.class.isAssignableFrom(cls);
    }




    public static void main(String[] ars){
        System.err.println(ReflectUtils.getPropertyNames(Thread.class));

    }
}
