package org.learn.develop.utils;

public class StringUtils {


    public static String[] split(String source,String splitor){
        if (null == source) {
            return null;
        }
        return source.split(splitor);

    }

    public static String splitAndGet(String source,String splitor,int index) throws Exception {
        String[] slice = split(source,splitor);
        if(slice == null)
            return null;

        if(index == -1){
            return slice[slice.length-1];
        }
        try {
            return slice[index];
        }catch (Exception e){
            new Exception("out of bound \t"+slice.length).printStackTrace();
        }
        return null;
    }



    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }



}
