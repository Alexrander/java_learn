
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeUtils {
    private TimeUtils(){}
    private static class TimeBuilder{
        Calendar calendar;
        TimeBuilder(){ this.calendar = Calendar.getInstance();}

        TimeBuilder from(long millisecond){
            this.calendar.setTime(new Date((millisecond)));
            return this;
        }
        TimeBuilder addMillisecond(int millisecond){
            this.calendar.add(Calendar.MILLISECOND, millisecond);
            return this;
        }
        TimeBuilder addSecond(int second){
            this.calendar.add(Calendar.SECOND, second);
            return this;
        }

        TimeBuilder addMinute(int minute){
            this.calendar.add(Calendar.MINUTE, minute);
            return this;
        }
        TimeBuilder addHour(int hour){
            this.calendar.add(Calendar.HOUR, hour);
            return this;
        }

        TimeBuilder addDay(int day){
            this.calendar.add(Calendar.DATE, day);
            return this;
        }
        TimeBuilder addMonth(int month){
            this.calendar.add(Calendar.MONTH, month);
            return this;
        }
        TimeBuilder addYear(int year){
            this.calendar.add(Calendar.YEAR, year);
            return this;
        }


        long buildAsMillisecond(){
            return this.calendar.getTimeInMillis();
        }

        String buildAsFormat(String format){
            return new SimpleDateFormat(format).format(this.calendar.getTime());
        }

    }


    public static TimeBuilder creatBuilder(){
        return new TimeBuilder();
    }


    public static void main(String[] args){

        String as = TimeUtils.creatBuilder().addDay(3).addMonth(4).buildAsFormat("yyyy.MM.dd");
        System.err.println(as);
    }
}
