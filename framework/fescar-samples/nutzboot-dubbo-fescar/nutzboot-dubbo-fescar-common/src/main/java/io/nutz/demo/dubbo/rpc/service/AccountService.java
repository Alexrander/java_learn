package io.nutz.demo.dubbo.rpc.service;
public interface AccountService {

    /**
     * debit balance of user'索引_数据结构_SynchronousQueue account
     */
    void debit(String userId, int money);
}