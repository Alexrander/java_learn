package org.learn.naive.dubbo;

import com.biz.soa.service.promotion.backend.game.GameUserStateDubboService;
import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ReferenceConfig;
import org.apache.dubbo.config.RegistryConfig;

public class GameTest {


    public static void main(String[] args) throws Exception {

        ApplicationConfig application = new ApplicationConfig();
        application.setName("kh-aliapplet");

        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("zookeeper://192.168.20.4:2181");


        ReferenceConfig<GameUserStateDubboService> reference = new ReferenceConfig<GameUserStateDubboService>();

        reference.setApplication(application);
        reference.setRegistry(registry); // 多个注册中心可以用setRegistries()
        reference.setInterface(GameUserStateDubboService.class);


        GameUserStateDubboService xxxService = reference.get();

        System.err.println(xxxService.sign(124L));
    }

}
