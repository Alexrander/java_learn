package org.learn.naive.dubbo.provider;

import org.apache.dubbo.config.ApplicationConfig;
import org.apache.dubbo.config.ProtocolConfig;
import org.apache.dubbo.config.RegistryConfig;
import org.apache.dubbo.config.ServiceConfig;
import org.learn.naive.dubbo.api.Order;
import org.learn.naive.dubbo.api.OrderImpl;

public class Provider {

    public static void main(String[] args){
        // 服务实现
        Order xxxService = new OrderImpl();

        // 当前应用配置
        ApplicationConfig application = new ApplicationConfig();
        application.setName("xxx");

// 连接注册中心配置
        RegistryConfig registry = new RegistryConfig();
        registry.setAddress("10.20.130.230:9090");


// 服务提供者协议配置
        ProtocolConfig protocol = new ProtocolConfig();
        protocol.setName("dubbo");
        protocol.setPort(12345);
        protocol.setThreads(200);


        // 服务提供者暴露服务配置
        ServiceConfig<Order> service = new ServiceConfig<Order>(); // 此实例很重，封装了与注册中心的连接，请自行缓存，否则可能造成内存和连接泄漏
        service.setApplication(application);
        service.setRegistry(registry); // 多个注册中心可以用setRegistries()
        service.setProtocol(protocol); // 多个协议可以用setProtocols()
        service.setInterface(Order.class);
        service.setRef(xxxService);
        service.setVersion("1.0.0");

// 暴露及注册服务
        service.export();


    }
}
