package org.learn.naive.api.es;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;

import java.net.InetAddress;

public class Connect {
    static String clusterName = "bbc-test";
    static Client client;

    public static TransportClient init() throws Exception{

        String clusterName = Connect.clusterName;

        Settings settings = Settings.settingsBuilder().put("cluster.name", clusterName).build();

        client = TransportClient.builder().settings(settings).build();


        ((TransportClient)client)
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("192.168.20.4"), 9300))
                .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName("192.168.20.3"), 9300))
                ;

        return (TransportClient) client;
    }

}
