package org.learn.naive.api.es;

import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Client;
import org.learn.naive.api.es.entity.LogEsEntity;



public class Main {



    public static void main(String[] args) throws Exception {
        Client client = Connect.init();


        LogEsEntity logEsEntity = new LogEsEntity();
        logEsEntity.setLogInfo("测试日志");


        IndexResponse response1 = client.prepareIndex("log_db", "log_table")
                .setSource(JSON.toJSONString(logEsEntity))
                .get();


    }
}
