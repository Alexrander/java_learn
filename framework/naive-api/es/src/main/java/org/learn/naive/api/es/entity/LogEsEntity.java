package org.learn.naive.api.es.entity;

import java.io.Serializable;
import java.util.Date;

public class LogEsEntity implements Serializable {

    private Date createTime;
    private String  logInfo;


    public LogEsEntity(){
        this.createTime = new Date();
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getLogInfo() {
        return logInfo;
    }

    public void setLogInfo(String logInfo) {
        this.logInfo = logInfo;
    }


}
