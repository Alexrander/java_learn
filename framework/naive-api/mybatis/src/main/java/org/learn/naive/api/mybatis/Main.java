package org.learn.naive.api.mybatis;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.learn.naive.api.mybatis.entity.User;
import org.learn.naive.api.mybatis.repository.MybatisUserRepository;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

public class Main {

    public static void main(String[] args) throws IOException {
        String resource = "META-INF/mybatis-config.xml";
        InputStream inputStream = Resources.getResourceAsStream(resource);
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession session = sqlSessionFactory.openSession();



        MybatisUserRepository mybatisUserRepository = session.getMapper(MybatisUserRepository.class);
        mybatisUserRepository.insert(new User(1));
    }
}
