package org.learn.naive.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.exception.NacosException;

public class PubConfig {

    public static void main(String[] args) throws NacosException {

        ConfigService configService = NacosFactory.createConfigService(Constant.ip);
        System.err.println(configService.publishConfig(Constant.dataId,Constant.group,"123123"));
    }

}
