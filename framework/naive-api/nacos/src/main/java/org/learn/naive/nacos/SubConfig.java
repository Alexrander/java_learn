package org.learn.naive.nacos;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;

import java.util.concurrent.Executor;
import java.util.concurrent.locks.LockSupport;

public class SubConfig {

    public static void main(String[] args) throws NacosException {
        ConfigService configService = NacosFactory.createConfigService(Constant.ip);


        configService.addListener(Constant.dataId, Constant.group, new Listener() {
            @Override
            public void receiveConfigInfo(String configInfo) {
                System.out.println("recieve1:" + configInfo);
            }
            @Override
            public Executor getExecutor() {
                return null;
            }
        });

        LockSupport.park();
    }
}
