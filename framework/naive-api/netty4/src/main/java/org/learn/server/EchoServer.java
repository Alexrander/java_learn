package org.learn.server;


import io.netty.channel.socket.SocketChannel;
import org.learn.ResHandle;
import org.learn.supports.netty.handlers.EchoHandle;
import org.learn.supports.netty.handlers.StringCodecHandle;
import org.learn.supports.netty.rpc.AbstractRpcRemotingServer;


public class EchoServer extends AbstractRpcRemotingServer {



    public static void main(String[] args) throws Exception {
        new EchoServer().start();


    }


    @Override
    public void staring() {
            System.err.println("server staring");
    }

    @Override
    public void socketChannelProcess(SocketChannel socketChannel) {
        socketChannel.pipeline()
                .addLast(new StringCodecHandle())
                .addLast(new EchoHandle())
                .addLast(new ResHandle())
        ;
//                .addLast(new CustomerDecoder())
//                .addLast(new ServerHandler());

    }

    @Override
    public void started() {
        System.err.println("server started");
    }
}