package org.learn.server.lowapi;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.channel.nio.NioEventLoop;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import java.net.InetSocketAddress;


public class ServerSocket {

    public static void main(String[] args){
        ServerBootstrap serverBootstrap = new ServerBootstrap();

        NioServerSocketChannel nioServerSocketChannel = new NioServerSocketChannel();

        NioEventLoopGroup nioEventLoopGroup = new NioEventLoopGroup();
        NioEventLoop eventExecutors ;


        nioEventLoopGroup.register(nioServerSocketChannel);


        nioServerSocketChannel.pipeline().addLast(
                new ChannelDuplexHandler(){
                    @Override
                    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                        System.err.println("三次握手");
                        ctx.fireChannelRead(msg);
                    }

                }
        );

        nioServerSocketChannel.bind(new InetSocketAddress(9999));



    }
}
