package org.learn.demo;

import org.apache.rocketmq.client.producer.DefaultMQProducer;

public class Producer {

    public static void main(String[] args){
        //Instantiate with a producer group name.
        DefaultMQProducer producer = new DefaultMQProducer("please_rename_unique_group_name");
        // Specify name server addresses.
        producer.setNamesrvAddr("192.168.6.129:9876");

    }
}