package com.work;


import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@EnableDubbo
@SpringBootApplication
public class AccountApp implements CommandLineRunner {
    public static void main(String[] args){
        SpringApplication.run(AccountApp.class);
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println("account dubbo started");

    }
}
