package com.work.dubbo;

import com.alibaba.dubbo.config.annotation.Service;
import com.work.account.AccountDubboService;
import com.work.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;


@Service
public class AccountApiService implements AccountDubboService {

    @Autowired
    AccountService accountService;

    @Override
    public boolean decreaseAccount(Double amount) {
        return accountService.decreaseAccount();
    }
}
