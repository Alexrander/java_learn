package com.work.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;



@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "account_tbl")
@Data
public class TAccount  extends BaseEntity{

    private String userId;
    private Double amount;


}
