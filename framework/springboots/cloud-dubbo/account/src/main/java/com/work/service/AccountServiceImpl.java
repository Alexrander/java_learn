package com.work.service;

import com.work.dao.TAccountDao;
import com.work.entity.TAccount;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService{

    @Autowired
    TAccountDao tAccountDao;

    public boolean decreaseAccount(){

        TAccount tAccount = new TAccount();
        tAccount.setId(123L);
        tAccount.setAmount(Math.random()*100);
        tAccount.setUserId("44444");
        tAccountDao.save(tAccount);
        return true;
    }



}
