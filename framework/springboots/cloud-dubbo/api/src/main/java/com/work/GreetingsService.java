package com.work;

public interface GreetingsService {
    String sayHello(String name);
}