package com.work.order;

import com.work.response.ObjectResponse;
import com.work.vo.OrderDTO;

public interface OrderDubboService {
    ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO);
}
