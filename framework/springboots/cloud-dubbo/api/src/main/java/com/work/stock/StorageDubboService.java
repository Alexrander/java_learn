package com.work.stock;


import com.work.response.ObjectResponse;
import com.work.vo.CommodityDTO;



public interface StorageDubboService {
    ObjectResponse decreaseStorage(CommodityDTO commodityDTO);
}
