package com.work;


import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.work.account.AccountDubboService;
import com.work.order.OrderDubboService;
import com.work.stock.StorageDubboService;
import com.work.vo.CommodityDTO;
import com.work.vo.OrderDTO;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.transaction.Transactional;
import java.math.BigDecimal;

@EnableDubbo
@Transactional
public class Client implements CommandLineRunner {


    public static void main(String[] args) {
        SpringApplication.run(Client.class, args);
    }


    @Reference
    StorageDubboService storageDubboService;
    @Reference
    OrderDubboService orderDubboService;
    @Reference
    AccountDubboService accountDubboService;



    private void callStock(){
        CommodityDTO commodityDTO = new CommodityDTO();
        commodityDTO.setId(3L);
        commodityDTO.setCount((int) (Math.random()*300));
        commodityDTO.setName("商品");
        commodityDTO.setCommodityCode("stock");
        storageDubboService.decreaseStorage(commodityDTO);

    }

    private void callOrder(){
        OrderDTO orderDTO = new OrderDTO();
        orderDTO.setUserId("123");
        orderDTO.setOrderAmount(new BigDecimal(Math.random()*500));
        orderDTO.setOrderCount((int) (Math.random() * 300));
        orderDTO.setOrderNo(""+Math.random());
        orderDTO.setCommodityCode("3");
        orderDubboService.createOrder(orderDTO);

    }

    private void callAccount() throws Exception {
        throw new Exception();
//        accountDubboService.decreaseAccount(1.0);
    }


    @Override
    @Transactional
    public void run(String... args) throws Exception {
//        accountApi.decreaseAccount(123.0);

//        System.err.println(greetingsService.sayHello("luohao"));
        this.callStock();
        this.callOrder();
        this.callAccount();
        System.exit(0);
    }
}
