package com.work.response;


public class ObjectResponse<T> extends BaseResponse{

    private T data;
    public T getData() {
        return data;
    }
    public void setData(T data) {
        this.data = data;
    }

}
