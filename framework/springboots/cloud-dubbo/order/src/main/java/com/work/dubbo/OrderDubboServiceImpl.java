package com.work.dubbo;


import com.alibaba.dubbo.config.annotation.Service;
//import com.alibaba.fescar.core.context.RootContext;
import com.work.order.OrderDubboService;
import com.work.response.ObjectResponse;
import com.work.service.OrderService;
import com.work.vo.OrderDTO;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: heshouyou
 * @Description
 * @Date Created in 2019/1/23 15:59
 */
@Service
public class OrderDubboServiceImpl implements OrderDubboService {

    @Autowired
    private OrderService orderService;

    @Override
    public ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO) {
//        System.out.println("rpc order 全局事务id ：" + RootContext.getXID());
        return orderService.createOrder(orderDTO);
    }
}
