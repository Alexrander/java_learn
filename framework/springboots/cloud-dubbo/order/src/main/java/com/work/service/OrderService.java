package com.work.service;

import com.work.response.ObjectResponse;
import com.work.vo.OrderDTO;

public interface OrderService {
    ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO);
}
