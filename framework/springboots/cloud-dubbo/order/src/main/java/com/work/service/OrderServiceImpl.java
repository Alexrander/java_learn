package com.work.service;

import com.work.dao.TOrderDao;
import com.work.entity.TOrder;
import com.work.response.ObjectResponse;
import com.work.vo.OrderDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    TOrderDao tOrderDao;

    @Override
    public ObjectResponse<OrderDTO> createOrder(OrderDTO orderDTO) {
        TOrder tOrder = new TOrder();
        ObjectResponse<OrderDTO> tOrderObjectResponse = new ObjectResponse<>();
        BeanUtils.copyProperties(orderDTO,tOrder);
        tOrder.setId(2L);

        try {
            tOrderDao.save(tOrder);
            tOrderObjectResponse.setData(orderDTO);
        }catch (Exception e){
            tOrderObjectResponse.setMessage("错误");
        }

        return tOrderObjectResponse;
    }
}
