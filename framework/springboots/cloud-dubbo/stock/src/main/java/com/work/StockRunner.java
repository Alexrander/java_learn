package com.work;

import com.alibaba.dubbo.config.spring.context.annotation.EnableDubbo;
import com.work.service.StockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class StockRunner implements CommandLineRunner {
    @Autowired
    StockService stockService;

    @Override
    public void run(String... args) throws Exception {
        System.err.println("stock dubbo started");
    }

    public static void main(String[] args){
        SpringApplication.run(StockRunner.class,args);

    }
}
