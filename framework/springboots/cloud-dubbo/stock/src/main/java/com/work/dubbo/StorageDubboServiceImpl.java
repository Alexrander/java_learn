package com.work.dubbo;


//import com.alibaba.fescar.core.context.RootContext;
import com.alibaba.dubbo.config.annotation.Service;
import com.work.response.ObjectResponse;
import com.work.service.StockService;
import com.work.stock.StorageDubboService;
import com.work.vo.CommodityDTO;
//import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @Author: heshouyou
 * @Description
 * @Date Created in 2019/1/23 16:13
 */
@Service
public class StorageDubboServiceImpl implements StorageDubboService {

    @Autowired
    private StockService stockService;

    @Override
    public ObjectResponse decreaseStorage(CommodityDTO commodityDTO) {
//        System.out.println("rpc stock 全局事务id ：" + RootContext.getXID());
        return stockService.decreaseStorage(commodityDTO);
    }
}
