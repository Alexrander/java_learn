package com.work.entity;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "storage_tbl")
public class TStorage extends  BaseEntity {


    private String commodityCode;
    private String name;
    private Integer count;

}
