package com.work.service;

import com.work.response.ObjectResponse;
import com.work.vo.CommodityDTO;

public interface StockService {

    ObjectResponse decreaseStorage(CommodityDTO commodityDTO);
}
