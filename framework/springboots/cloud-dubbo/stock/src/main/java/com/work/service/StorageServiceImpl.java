package com.work.service;


import com.work.dao.StockDao;
import com.work.entity.TStorage;
import com.work.response.ObjectResponse;
import com.work.vo.CommodityDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class StorageServiceImpl  implements StockService {
    @Autowired
    StockDao stockDao;


    @Override
    public ObjectResponse decreaseStorage(CommodityDTO commodityDTO) {
        ObjectResponse<Object> response = new ObjectResponse<>();
        Optional<TStorage> optionalTStorage = stockDao.findById(3L);
        TStorage tStorage = optionalTStorage.orElseGet(TStorage::new);
        BeanUtils.copyProperties(commodityDTO,tStorage);
        stockDao.save(tStorage);

        return response;
    }

}
