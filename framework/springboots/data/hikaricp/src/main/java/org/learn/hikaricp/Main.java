package org.learn.hikaricp;

import org.learn.hikaricp.api.User;
import org.learn.hikaricp.dao.DaoServer;
import org.learn.hikaricp.dao.UserEntityRepositroy;
import org.learn.hikaricp.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;


@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    DaoServer dao;
    @Autowired
    UserEntityRepositroy userEntityRepositroy;



    public static void main(String[] args) {
        SpringApplication.run(Main.class);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        UserEntity user = new UserEntity();
        System.err.println(user.getClass().getClassLoader());

    }


    void testApiSave(){
//
        System.err.println("not impl yet you must rewrite the repository function etc:saveAll");
        UserEntity user = new UserEntity();
        System.err.println(user.getClass().getClassLoader());
        user.setUserId(123);
        user.setPwd("pwdd");
        user.setUserName("那么");
        user.setAssistedQueryPwd("3214");
        userEntityRepositroy.save(user);

    }
    void testProjection(){
        System.err.println(dao.test().get(0).getUserId());
    }



}
