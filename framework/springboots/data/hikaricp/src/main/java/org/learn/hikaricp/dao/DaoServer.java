package org.learn.hikaricp.dao;

import org.learn.hikaricp.entity.OrderTbl;
import org.learn.hikaricp.projection.CustomerProjection;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;


@Repository
public interface DaoServer extends JpaRepository<OrderTbl, Integer> {


    @Query(value = "select  user_id as userId,commodity_code as commodityCode from order_tbl;",nativeQuery = true)
    List<CustomerProjection> test();
}
