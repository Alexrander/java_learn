package org.learn.hikaricp.dao;

import org.learn.hikaricp.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserEntityRepositroy extends CrudRepository<UserEntity, Long> {
}
