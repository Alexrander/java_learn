package org.learn.hikaricp.projection;

public interface CustomerProjection {

    String getUserId();
    String getCommodityCode();


}
