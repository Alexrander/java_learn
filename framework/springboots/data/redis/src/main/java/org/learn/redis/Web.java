package org.learn.redis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Web  {

    @RequestMapping("/")
    public String test(){
        System.err.println(Thread.currentThread().getName());
        return "{\"name\":\"hello\"}";
    }

    public static void main(String[] args){
        SpringApplication.run(Web.class);
    }
}
