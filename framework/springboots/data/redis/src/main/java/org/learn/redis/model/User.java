package org.learn.redis.model;


import org.learn.develop.common.annotations.redis.Ro;

import java.io.Serializable;

@Ro(prefix = "user")
public class User implements Serializable {
    String id = "123";
    String name = "luohao";
    String addr = "江南";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }
}
