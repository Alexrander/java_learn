package org.learn.shardingjdbc;
import org.learn.shardingjdbc.service.CommonServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
public class ShardingApp implements CommandLineRunner {

    @Autowired
    CommonServiceImpl commonService;



    public static void main(final String[] args) {
        SpringApplication.run(ShardingApp.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        List<Long> ids = commonService.createOrderAndOrderItem();
        System.err.println(ids);
        System.err.println(commonService.findAll().size());


    }

}
