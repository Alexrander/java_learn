package org.learn.shardingjdbc.repositroy;


import org.learn.shardingjdbc.entity.OrderItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * 分片操作 的代理不再事务中 手动添加
 */
@Repository
@Transactional
public interface OrderItemRepository extends JpaRepository<OrderItemEntity,Long> {


    void deleteByOrderId(Long orderId);

}
