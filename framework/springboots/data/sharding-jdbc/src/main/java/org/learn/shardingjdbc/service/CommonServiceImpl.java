/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.learn.shardingjdbc.service;

import org.learn.shardingjdbc.entity.OrderEntity;
import org.learn.shardingjdbc.entity.OrderItemEntity;
import org.learn.shardingjdbc.repositroy.OrderItemRepository;
import org.learn.shardingjdbc.repositroy.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommonServiceImpl {
    
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderItemRepository orderItemRepository;


    
    @Transactional
    public List<Long> createOrderAndOrderItem() {
        List<Long> result = new ArrayList<>(10);
        for (int i = 1; i <= 10; i++) {
            OrderEntity orderEntity = OrderEntity.defalutOrder(i);
            orderRepository.save(orderEntity);

            OrderItemEntity orderItemEntity = OrderItemEntity.defalutOrderItem(orderEntity.getOrderId(),i);
            orderItemRepository.save(orderItemEntity);

            result.add(orderEntity.getOrderId());
        }
        return result;
    }


    public void deleteOrder(List<Long> orderIds) {
        orderIds.forEach(x -> {orderRepository.deleteById(x);});
    }

    public void deleteOrderItem(List<Long> orderIds) {
        orderIds.forEach(x -> {orderItemRepository.deleteByOrderId(x);});
    }




    public void deleteOrderAndItem(List<Long> orderIds) {
        for (Long each : orderIds) {
            orderRepository.deleteById(each);
            orderItemRepository.deleteByOrderId(each);
        }
    }

    public List<OrderItemEntity> findAll(){
        return orderItemRepository.findAll();
    }
    

}
