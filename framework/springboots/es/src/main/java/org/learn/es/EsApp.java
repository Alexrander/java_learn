package org.learn.es;

import org.learn.es.entity.Goods;
import org.learn.es.repository.GoodsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EsApp implements CommandLineRunner {
    @Autowired
    GoodsRepository goodsRepository;
    public static void main(String[] args) {
        SpringApplication.run(EsApp.class);
    }


    public void add() {
        Goods employee = new Goods();
        employee.setId(System.currentTimeMillis());
        employee.setName("LUO");
        employee.setDescription("这是 quanwen 检索");
        goodsRepository.save(employee);
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println("ES Client");
        add();

    }
}
