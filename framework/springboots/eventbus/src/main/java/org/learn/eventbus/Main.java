package org.learn.eventbus;

import org.learn.eventbus.event.DemoEvent;
import org.learn.eventbus.service.SampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    SampleService sampleService;

    public static void main(String[] args){

        SpringApplication.run(Main.class,args);


    }

    @Override
    public void run(String... args) throws Exception {
            sampleService.publishEvent(new DemoEvent(this));
    }
}
