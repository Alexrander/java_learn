package org.learn.eventbus.listenr;


import org.learn.eventbus.event.DemoEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class DemoEventListener implements ApplicationListener<DemoEvent> {

    //使用注解@Async支持 这样不仅可以支持通过调用，也支持异步调用，非常的灵活
    @Async
    @Override
    public void onApplicationEvent(DemoEvent event) {
        System.out.println("注册成功，发送确认邮件为：");
    }

}