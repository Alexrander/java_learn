package org.learn.eventbus.service;


import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;



public abstract class AbstractBaseService implements ApplicationEventPublisherAware {
    /**
     * 使用spring事件总线发布事件
     * 使用@TransactionalEventListener注解监听事件来达到"事务提交之后处理事件"的效果
     * 替代原本的四种事件发布方式
     *
     * @param event
     */
    private ApplicationEventPublisher applicationEventPublisher;



    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }



    public void publishEvent(ApplicationEvent event) {
        this.applicationEventPublisher.publishEvent(event);
    }



}
