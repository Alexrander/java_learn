package org.learn.rabbit.api.vo;

import java.io.Serializable;


public class User implements Serializable {
    private String name;
    private String phone;

    public User(){}
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
