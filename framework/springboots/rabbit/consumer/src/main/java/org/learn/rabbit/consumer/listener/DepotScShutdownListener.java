package org.learn.rabbit.consumer.listener;

import com.rabbitmq.client.Channel;
import org.learn.rabbit.api.vo.User;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
@RabbitListener(queues = "depotScShutdown")
public class DepotScShutdownListener {


    @RabbitHandler
    public void handleMessage(User user, Channel channel, @Header(AmqpHeaders.DELIVERY_TAG) long deliveryTag){

        System.out.println("消费消息\t"+user.getName()+"\t"+user.getPhone());
        System.out.println(deliveryTag);

        try {
            channel.basicAck(deliveryTag,false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
