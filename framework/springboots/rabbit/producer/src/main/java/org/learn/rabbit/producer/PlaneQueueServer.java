package org.learn.rabbit.producer;

import org.learn.rabbit.api.vo.User;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlaneQueueServer {
    @Autowired
    AmqpTemplate rabbitmqTemplate;


    public boolean planeSend(){
        User user = new User();
        user.setName("罗浩");
        user.setPhone("155000");
        rabbitmqTemplate.convertAndSend("com.biz.base.direct","testQueue",user);
        System.err.println("***send***");
        return true;
    }

}
