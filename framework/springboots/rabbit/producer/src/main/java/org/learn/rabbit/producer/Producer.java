package org.learn.rabbit.producer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;





@SpringBootApplication
public class Producer implements CommandLineRunner {
    @Autowired
    PlaneQueueServer planeSend;



    @Override
    public void run(String... strings) {
        planeSend.planeSend();

    }

    public static void main(String[] args){

        SpringApplication.run(Producer.class, args);

    }

}
