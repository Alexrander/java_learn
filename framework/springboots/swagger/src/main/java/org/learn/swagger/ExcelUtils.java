package org.learn.swagger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 建议使用alibababa   easyexcell
 */
public class ExcelUtils {


    private ExcelUtils(){}

    interface ResolverMapper<T>{
         T doResolve(Row row, Class<T> clazz);
    }

//
//    private static class RowHolder{
//
//        private Row row;
//
//        RowHolder(Row row){
//            assert row!=null;
//            this.row = row;
//        }
//
//    }


    public static class ExcelBuilder{
        /**
         * 仅仅支持第一页 没有空扩展
         *
         * */
        private XSSFWorkbook workbook;
        private XSSFSheet sheets;
        private AtomicInteger rowId = new AtomicInteger(0);
        private ResolverMapper resolver;

        ExcelBuilder(XSSFWorkbook xssfWorkbook){
            this.workbook = xssfWorkbook;
            try {
                this.sheets = xssfWorkbook.getSheetAt(0);
            }catch (Exception e){
                this.sheets = xssfWorkbook.createSheet();
            }
            System.err.println("加载sheets:\t"+this.sheets.getLastRowNum()+"\t 行");

        }


        public ExcelBuilder addRow(String[] cells){
            /**
             * 仅支持String
             */
            Row row = this.sheets.createRow(this.rowId.getAndAdd(1));
            for(int i=0;i<cells.length;i++){
                row.createCell(i).setCellValue(cells[i]);
            }


            return this;
        }

        public ExcelBuilder setResolver(ResolverMapper resolver){
            this.resolver = resolver;
            return this;
        }



        public List buildByResolverPassException() throws Exception {
            if(this.resolver == null)
                throw new Exception("empty resolver");
            List res = new ArrayList();
            for(int i=1; i<this.sheets.getLastRowNum();i++){
                System.err.println(this.sheets.getRow(i));
            }

            return res;
        }




        public void buildToFile(String path) throws IOException {
            File file = new File(path);
            FileOutputStream out = new FileOutputStream(file);
            this.workbook.write(out);
            out.close();
            System.out.println("has writed");
        }



        public OutputStream buildToStream(OutputStream os) throws IOException {
            this.workbook.write(os);
            return os;
        }


        public  Integer getSize () {
            return this.sheets.getLastRowNum();
        }


        public  List<Row> getRowsSkipHead () {
            List<Row> rows = new ArrayList<>();
            for (int i = 1; i < this.sheets.getLastRowNum()+1; i++) {
                    rows.add(this.sheets.getRow(i));
            }
            return rows;
        }




    }


    private static XSSFWorkbook createExcel(){
            return new XSSFWorkbook();
    }

    private static XSSFWorkbook createExcel(InputStream inputStream) throws IOException {
        return new XSSFWorkbook(inputStream);
    }






    /********************************************************************************************************************
     *  Core Api
    ********************************************************************************************************************/

    public static ExcelBuilder createExcelBuilder(){
        return new ExcelBuilder(createExcel());

    }

    public static ExcelBuilder createExcelBuilder(String url) throws IOException {
        return new ExcelBuilder(createExcel(IOUtils.readFrom(url)));

    }



//    public static <T> List<T> resolvePassException(String url,Class<T> clazz){
//        return new ExcelBuilder(createExcel(number));
//    }


    public static void main(String[] args) throws IOException {
         String oss ="https://1919-new-bbc-test.oss-cn-beijing.aliyuncs.com/0e44b829-d565-42bb-86c3-2047655078c2";
         String toFile="/home/luohao/Diablo/bar/java_learn/develop/utils/src/main/java/org/learn/develop/utils/Test2.xlsx";
//        ExcelUtils.createExcelBuilder()
//                .addRow(new String[]{"标题1","标题2"})
//                .addRow(new String[]{"1","2"})
//                .addRow(new String[]{"3","4"})
//                .buildToFile("/home/luohao/Diablo/bar/java_learn/develop/utils/src/main/java/org/learn/develop/utils/Test2.xlsx");


    }
}
