package org.learn.swagger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class IOUtils {
    private IOUtils(){}



    public static InputStream readFrom(String url) throws IOException {
        URL remote = new URL(url);
        return remote.openConnection().getInputStream();

    }


    public static void main(String[] args){

    }
}
