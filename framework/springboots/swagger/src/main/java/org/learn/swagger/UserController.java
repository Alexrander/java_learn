package org.learn.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;

//@RestController
@Controller
public class UserController {
    @RequestMapping("/")
    public void test(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String nameStr ="题库模板(1).xlsx";
            String fileName = URLEncoder.encode(nameStr, "utf-8");

            response.setContentType("application/vnd.ms-excel; charset=utf-8");
            //附件下载，不同浏览器是否选择路径不同效果，火狐会选路径
            response.setHeader("Content-disposition", "attachment;filename=" + fileName);
            response.setCharacterEncoding("utf-8");

            OutputStream os = ExcelUtils.createExcelBuilder("https://1919-new-bbc-test.oss-cn-beijing.aliyuncs.com/4dd37626-cf4e-492c-88ea-f62ddd06d5af").buildToStream(response.getOutputStream());
            os.flush();
            os.close();

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


}
