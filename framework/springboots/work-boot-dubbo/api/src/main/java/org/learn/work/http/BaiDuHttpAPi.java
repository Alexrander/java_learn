package org.learn.work.http;



import com.oneapm.touch.retrofit.boot.annotation.RetrofitService;
import retrofit2.Call;
import retrofit2.http.GET;

@RetrofitService("baidu")
public interface BaiDuHttpAPi {

    @GET("/")
    Call<Object> contributors();

}
