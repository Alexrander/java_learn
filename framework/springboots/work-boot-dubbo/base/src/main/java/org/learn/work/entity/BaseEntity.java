package org.learn.work.entity;



import lombok.Data;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@SuppressWarnings("serial")
@MappedSuperclass
@Data
public abstract class BaseEntity implements Serializable {
    @Id
    private Long id;
}
