package org.learn.work.http;



import com.oneapm.touch.retrofit.boot.RetrofitServiceScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RetrofitServiceScan
public class HttpClient implements CommandLineRunner {

    @Autowired
    BaiDuHttpAPi baiDuHttpApi;

    public static void main(String[] args) {
        SpringApplication.run(HttpClient.class);
    }

    @Override
    public void run(String... args) throws Exception {
        System.err.println(baiDuHttpApi.contributors().execute());

    }
}
