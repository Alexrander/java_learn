package org.learn.work;


import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class PayDubbo {
    public static void main(String[] args){
        SpringApplication.run(PayDubbo.class);
        System.err.println("pay dubbo started");
    }

}
