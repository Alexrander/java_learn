package org.learn.work;

import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableDubbo
public class Client implements CommandLineRunner {

    @Reference
    PayDubboApi payDubboApi;
    @Reference
    StockDubboApi stockDubboApi;

    public static void main(String[] args){
        SpringApplication.run(Client.class);
    }


    @Override
    public void run(String... args) throws Exception {
        System.err.println("client  started");
        System.err.println(payDubboApi.doPay(1));
    }
}
