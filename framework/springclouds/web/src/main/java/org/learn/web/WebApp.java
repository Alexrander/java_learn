package org.learn.web;


import org.springframework.boot.SpringApplication;
import org.springframework.cloud.client.SpringCloudApplication;

@SpringCloudApplication
public class WebApp {
    public static void main(String[] args){
        SpringApplication.run(WebApp.class,args);
    }
}
