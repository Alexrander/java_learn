package org.learn.nio.demo;


import org.junit.Test;
import org.learn.nio.utils.ChannelCreator;


import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileChannelUse {

    @Test
    public void run() throws IOException {
        FileChannel fileChannel = ChannelCreator.createFileChannel("data.txt");
        ByteBuffer buf = ByteBuffer.allocate(8*8);


        int bytesRead = fileChannel.read(buf);

        while (bytesRead != -1) {
            System.out.println("Read " + bytesRead);
            buf.flip();
            while(buf.hasRemaining()){
                System.out.print((char) buf.get());
            }

            buf.clear();
            bytesRead = fileChannel.read(buf);
        }
        fileChannel.close();



    }


}
