package org.learn.nio.demo;

import com.sun.deploy.util.ArrayUtil;
import org.junit.Test;
import org.learn.nio.utils.ChannelCreator;

import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class GatherAndScattering {

    @Test
    public void run() throws IOException {
        FileChannel fileChannel = ChannelCreator.createFileChannel("data.txt");

        ByteBuffer header = ByteBuffer.allocate(8);
        ByteBuffer body   = ByteBuffer.allocate(8*24);

        ByteBuffer[] bufferArray = { header, body };

        fileChannel.read(bufferArray);
        header.flip();
        body.flip();
        while (header.hasRemaining())
            System.err.println((char) header.get());
        while (body.hasRemaining())
            System.err.println((char) body.get());



    }
}
