package org.learn.nio.demo;

import org.junit.Test;
import org.learn.nio.utils.ChannelCreator;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class SelectorUse {

    static int port = 9999;
    @Test
    public void run() throws IOException, InterruptedException {
        SocketChannel socketChannel = ChannelCreator.createSocketChannel();

        socketChannel.connect(new InetSocketAddress("127.0.0.1", port));

        socketChannel.configureBlocking(false);

        ByteBuffer buf = ByteBuffer.allocate(1024);

        int flag = socketChannel.read(buf);
        System.err.println(flag);
        socketChannel.close();

    }


    @Test
    public void server() throws IOException {
        ServerSocketChannel serverSocketChannel = ChannelCreator.createServerSocketChannel();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));


        for (;;){
            SocketChannel socketChannel = serverSocketChannel.accept();

            System.err.println("client call");
        }




    }

}
