package org.learn.nio.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ChannelCreator {

    public static FileChannel createFileChannel(String path) throws FileNotFoundException {
        return new RandomAccessFile(path, "rw").getChannel();
    }
    public static SocketChannel createSocketChannel() throws IOException {
        return SocketChannel.open();
    }
    public static ServerSocketChannel createServerSocketChannel() throws IOException {
        return ServerSocketChannel.open();
    }
}
