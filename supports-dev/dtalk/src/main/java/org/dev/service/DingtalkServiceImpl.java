package org.dev.service;



import java.sql.Timestamp;







public class DingtalkServiceImpl{

    // 完成重置全量推送的群
    private static final String ROBOT_PATROL_OSS_FULL_DATA = "https://oapi.dingtalk.com/robot/send?access_token=3f4e36a13cbba47e21425bb4cc1ec84f71a3db8725d53d8c326c768db167f44a";



    public void sendOssFullDataPatrolResult(String desc , Timestamp  startTime , Timestamp endTime , long usedTime) {
        DingTalkClient client = new DefaultDingTalkClient(ROBOT_PATROL_OSS_FULL_DATA);
        OapiRobotSendRequest request = new OapiRobotSendRequest();


        request.setMsgtype("markdown");
        OapiRobotSendRequest.Markdown markdown = new OapiRobotSendRequest.Markdown();
        String title = "完成重置全量"+desc;

        StringBuilder builder = new StringBuilder();
        builder.append("**");
        builder.append(title);
        builder.append("**\n");
        builder.append("> 开始时间 ");
        builder.append(startTime);
        builder.append("\n > \n > 结束时间 ");
        builder.append(endTime);
        builder.append("\n > \n > 用时 ");
        builder.append(usedTime);
        builder.append(" ms");


        markdown.setTitle(title);
        markdown.setText(builder.toString());
        request.setMarkdown(markdown);
        try {
            OapiRobotSendResponse response = client.execute(request);
            logger.info("sending dingtalk robot msg , response code:{} , response body:{}  " , response.getCode() , response.getBody());
        }catch (ApiException exception){
            logger.error("failed to sending dingtalk robot msg ");
        }

    }



}
