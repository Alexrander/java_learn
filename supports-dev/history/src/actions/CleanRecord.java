package actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import toolbar.HistoryWin;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

public class CleanRecord extends AnAction {

    @Override
    public void actionPerformed(AnActionEvent e) {

        JTree jTree = HistoryWin.myToolWindow.getTree1();
        Record.rootNode =  new DefaultMutableTreeNode("Methods");
        jTree.setModel(new DefaultTreeModel(Record.rootNode));
        jTree.updateUI();

    }
}
