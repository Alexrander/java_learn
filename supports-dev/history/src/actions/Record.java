package actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.util.PsiTreeUtil;
import context.Context;
import context.CursorContext;
import context.FileContext;
import context.RuntimeContext;
import toolbar.HistoryWin;
import utils.PsiUtils;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.util.*;


public class Record extends AnAction {

    public static DefaultMutableTreeNode rootNode = new DefaultMutableTreeNode("Methods");



    @Override
    public void actionPerformed(AnActionEvent e) {
        // TODO: insert action logic here

        Context.reload(e);
        subscribe();
    }


    private void subscribe(){

        RuntimeContext.recordsMethod.put(CursorContext.getCurMethod().getName(),CursorContext.getCurMethod());
        rootNode.add( new DefaultMutableTreeNode(CursorContext.getCurMethod().getName()));
        reloadTree();

    }


    private void reloadTree(){

        JTree jTree = HistoryWin.myToolWindow.getTree1();
        jTree.setModel(new DefaultTreeModel(rootNode));
        jTree.updateUI();
    }


}
