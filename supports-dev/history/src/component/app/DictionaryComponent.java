package component.app;

import com.intellij.openapi.components.ApplicationComponent;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.event.EditorEventMulticaster;
import org.dev.listener.SelectListener;
import org.jetbrains.annotations.NotNull;


public class DictionaryComponent implements ApplicationComponent {
    private SelectListener selectListener;

    public DictionaryComponent() {
    }

    @Override
    public void initComponent() {
        selectListener = new SelectListener();
        System.out.println("component初始化--------");
        EditorEventMulticaster eventMulticaster = EditorFactory.getInstance().getEventMulticaster();
        eventMulticaster.addSelectionListener(selectListener);
        eventMulticaster.addEditorMouseListener(selectListener);
    }

    @Override
    public void disposeComponent() {
        System.out.println("component结束--------");
    }

    @Override
    @NotNull
    public String getComponentName() {
        return "DictionaryComponent";
    }


}
