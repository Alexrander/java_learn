package component.project;

import actions.Record;
import com.intellij.openapi.components.AbstractProjectComponent;
import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import context.Context;
import context.RuntimeContext;
import toolbar.HistoryWin;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class ContextComponent extends AbstractProjectComponent implements ProjectComponent {



    @Override
    public void  projectOpened(){
        JTree jTree = HistoryWin.myToolWindow.getTree1();
        jTree.setModel(new DefaultTreeModel(Record.rootNode));
        jTree.updateUI();

        jTree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                // 获取被选中的相关节点
                TreePath path = e.getPath();
                String name =  path.getPathComponent(path.getPathCount()-1).toString();
                RuntimeContext.recordsMethod.get(name).navigate(true);
            }
        });


    }


    protected ContextComponent(Project project) {
        super(project);
        Context.project = project;

    }





}
