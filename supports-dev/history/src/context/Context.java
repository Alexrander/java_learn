package context;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;

public class Context {

    public static   Project project;
    public static   Editor editor;
    public static   PsiFile psiFile;

    public static   CaretModel caretModel;

    public static PsiElement cursorElement;

    public static Document document;

    public static void reload(AnActionEvent e){
        project =  e.getData(CommonDataKeys.PROJECT);
        psiFile =  e.getData(CommonDataKeys.PSI_FILE);
        editor =  e.getData(CommonDataKeys.EDITOR);
        cursorElement = e.getData(LangDataKeys.TARGET_PSI_ELEMENT);

        if (editor == null)
            caretModel = null;
        else{
            caretModel = editor.getCaretModel();
            document = editor.getDocument();
        }


    }

    public static void printInfo() {
        System.err.println(project.getName()+"\t"+psiFile.getName());
    }
}
