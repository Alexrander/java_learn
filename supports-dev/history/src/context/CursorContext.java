package context;

import com.intellij.psi.PsiMethod;
import meta.OffSetMeta;

import java.util.ArrayList;
import java.util.List;

public class CursorContext extends Context{


    public static int getCurLineStartOffset(){
        return document.getLineStartOffset(getCurLine());
    }

    public static int getCurLineEndOffset(){
        return document.getLineEndOffset(getCurLine());
    }

    public static double getCurLineMeanOffset(){
        return  ((getCurLineStartOffset()+getCurLineEndOffset())/2.0) - 1.0 ;
    }




    public static int getCurLine(){
        return caretModel == null ?  -1: caretModel.getLogicalPosition().line+1;
    }

    public static PsiMethod getCurMethod(){

        List<OffSetMeta> offSetMetaList = new ArrayList<>();

        FileContext.getAllMethods().forEach(
                x->{
                    x.forEach(y->{
                            offSetMetaList.add(new OffSetMeta(y));
                    });
                }
        );


        for (OffSetMeta offSetMeta: offSetMetaList){
            if(offSetMeta.between(getCurLineMeanOffset())){
                return offSetMeta.getPsiMethod();
            }
        }


        return null;
    }



}
