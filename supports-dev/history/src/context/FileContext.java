package context;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.util.PsiTreeUtil;
import meta.RowMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class FileContext extends Context {




    public static List<PsiClass> getAllClasses(){
        if(psiFile == null)
            return new ArrayList<>();
        return PsiTreeUtil.getChildrenOfTypeAsList(Context.psiFile, PsiClass.class);
    }


    public static List<List<PsiMethod>> getAllMethods(){
            if(psiFile == null)
                return new ArrayList<>();


        return PsiTreeUtil.getChildrenOfTypeAsList(Context.psiFile, PsiClass.class).stream().map(x->
             PsiTreeUtil.getChildrenOfTypeAsList(x,PsiMethod.class)).collect(Collectors.toList());


    }





}
