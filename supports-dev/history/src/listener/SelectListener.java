package org.dev.listener;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.event.*;

public class SelectListener  implements SelectionListener, EditorMouseListener {

    @Override
    public void selectionChanged(SelectionEvent selectionEvent) {
//        选取文字变更 这个在拖动鼠标的时候 会持续变更
        Editor editor=selectionEvent.getEditor();
        String text=editor.getSelectionModel().getSelectedText();
//        System.err.println(text);
//        ProjectContext.UI.getTextArea1().append(text);

    }

    @Override
    public void mousePressed(EditorMouseEvent editorMouseEvent) {
//        System.err.println("mousePressed");

    }

    @Override
    public void mouseClicked(EditorMouseEvent editorMouseEvent) {
        Editor editor = editorMouseEvent.getEditor();

    }

    @Override
    public void mouseReleased(EditorMouseEvent editorMouseEvent) {

    }

    @Override
    public void mouseEntered(EditorMouseEvent editorMouseEvent) {


    }

    @Override
    public void mouseExited(EditorMouseEvent editorMouseEvent) {

    }
}
