package meta;

import com.intellij.psi.PsiClass;
import com.intellij.openapi.editor.Document;
import com.intellij.psi.PsiComment;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.search.searches.ClassInheritorsSearch;

import java.util.ArrayList;
import java.util.List;

public class ClassMeta {

    private PsiClass psiClass;

    private List<MethodMeta> methodMetas = new ArrayList<>();

    public ClassMeta(PsiClass psiClass){
//        ClassInheritorsSearch.search(psiClass);
        this.psiClass = psiClass;
        this.init();
    }


    private void init(){
        PsiMethod[] methods = this.psiClass.getMethods();
        for (PsiMethod psiMethod : methods) {
            this.methodMetas.add(new MethodMeta(psiMethod));
        }
    }

    /**
     * @author gaojindan
     * @date 2019/3/11 0011 17:08
     * @des 检查类的注释规则
     * @param  psiClass:元素
     * @return
     */
    public String getClassComment(Document document, PsiClass psiClass){
        PsiComment classComment = null;
        for (PsiElement tmpEle : psiClass.getChildren()) {
            if (tmpEle instanceof PsiComment){
                classComment = (PsiComment) tmpEle;
                String tmpText = classComment.getText();
                return tmpText;

            }
        }
        return null;
    }



    public List<MethodMeta> getMethodMetas(){
        return this.methodMetas;
    }


    public String getClassName(){
        return this.psiClass.getName();
    }
}
