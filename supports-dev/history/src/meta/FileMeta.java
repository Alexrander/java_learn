package meta;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;

import java.util.ArrayList;
import java.util.List;

public class FileMeta {


    private List<ClassMeta> classMetas = new ArrayList<>();
    private PsiFile psiFile;


    public FileMeta(PsiFile psiFile){
        this.psiFile = psiFile;
        init();
    }



    private void init(){
        if (psiFile == null)
            return;


        for (PsiElement psiElement : psiFile.getChildren()){
            if (psiElement instanceof PsiClass){
//                todo fixme  必须递归 否则只能有1个class
                this.classMetas.add(new ClassMeta((PsiClass) psiElement));
            }
        }
    }








    public List<ClassMeta> getClassMetas(){
        return this.classMetas;
    }




    public int getCusorLine(){

        return 0;
    }



}
