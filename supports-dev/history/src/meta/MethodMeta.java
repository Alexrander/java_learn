package meta;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;

import java.io.CharArrayReader;
import java.io.IOException;
import java.io.LineNumberReader;

import com.intellij.openapi.editor.Document;

public class MethodMeta{

    private PsiClass psiClass;
    private PsiMethod psiMethod;


    public MethodMeta(PsiMethod psiMethod){
        this.psiMethod = psiMethod;
    }

    public MethodMeta(PsiClass psiClass,PsiMethod psiMethod){
        this.psiMethod = psiMethod;
        this.psiClass = psiClass;
    }




    /**
     * 获取注释
     * @param document
     * @param psiMethod
     */
    public String getMethodComment(Document document, PsiMethod psiMethod) {
        PsiComment classComment = null;

        for (PsiElement tmpEle : psiMethod.getChildren()) {
            if (tmpEle instanceof PsiComment) {
                classComment = (PsiComment) tmpEle;

                String tmpText = classComment.getText();
                return tmpText;
            }
        }
        return null;
    }


    /**
     * 获取总行数
     * @return
     */
    public long getTotalLineNumber(){
        PsiCodeBlock psiCodeBlock = this.psiMethod.getBody();
        String codeText = psiCodeBlock.getText();
        LineNumberReader lnr = new LineNumberReader(new CharArrayReader(codeText.toCharArray()));
        try {
            lnr.skip(Long.MAX_VALUE);
            lnr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return lnr.getLineNumber() + 1;
    }


    public TextRange getTextRange(){
        return this.psiMethod.getTextRange();
    }



    public PsiClass getPsiClass() {
        return psiClass;
    }

    public void setPsiClass(PsiClass psiClass) {
        this.psiClass = psiClass;
    }

    public PsiMethod getPsiMethod() {
        return psiMethod;
    }

    public void setPsiMethod(PsiMethod psiMethod) {
        this.psiMethod = psiMethod;
    }
}
