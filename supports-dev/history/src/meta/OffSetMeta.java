package meta;

import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiMethod;

public class OffSetMeta {

    private int start;
    private int end;

    private PsiMethod psiMethod;
    private PsiClass psiClass;


    public OffSetMeta(int start,int end){
        this.start = start;
        this.end = end;
    }

    public OffSetMeta(PsiMethod psiMethod){
        assert  psiMethod != null;
        this.psiMethod = psiMethod;
        this.start = psiMethod.getTextRange().getStartOffset();
        this.end = psiMethod.getTextRange().getEndOffset();

    }

    public boolean between(double off){
        return (this.start<= off && off<=this.end);

    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getEnd() {
        return end;
    }

    public void setEnd(int end) {
        this.end = end;
    }

    public PsiMethod getPsiMethod() {
        return psiMethod;
    }

    public void setPsiMethod(PsiMethod psiMethod) {
        this.psiMethod = psiMethod;
    }

    public PsiClass getPsiClass() {
        return psiClass;
    }

    public void setPsiClass(PsiClass psiClass) {
        this.psiClass = psiClass;
    }
}
