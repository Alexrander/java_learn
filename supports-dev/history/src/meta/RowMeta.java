package meta;

import com.intellij.psi.PsiFile;

import java.util.HashMap;
import java.util.Map;

public class RowMeta {




    private  HashMap<Integer, RowMeta> rowMetaHashMap = new HashMap<>();

    private int totalRow;
    private MethodMeta methodMetaArea;
    private ClassMeta classMetaArea;


    public HashMap<Integer, RowMeta> getRowMetaHashMap() {
        return rowMetaHashMap;
    }

    public void setRowMetaHashMap(HashMap<Integer, RowMeta> rowMetaHashMap) {
        this.rowMetaHashMap = rowMetaHashMap;
    }

    public int getTotalRow() {
        return totalRow;
    }

    public void setTotalRow(int totalRow) {
        this.totalRow = totalRow;
    }

    public MethodMeta getMethodMetaArea() {
        return methodMetaArea;
    }

    public void setMethodMetaArea(MethodMeta methodMetaArea) {
        this.methodMetaArea = methodMetaArea;
    }

    public ClassMeta getClassMetaArea() {
        return classMetaArea;
    }

    public void setClassMetaArea(ClassMeta classMetaArea) {
        this.classMetaArea = classMetaArea;
    }
}
