package org.dev.service;

import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

public interface Tracer {
    static Tracer getInstance(@NotNull Project project) {
        return ServiceManager.getService(project, Tracer.class);
    }
}
