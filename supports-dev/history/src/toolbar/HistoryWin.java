package toolbar;

import actions.Record;
import com.intellij.openapi.editor.EditorFactory;
import com.intellij.openapi.editor.event.EditorEventMulticaster;
import com.intellij.openapi.fileEditor.FileEditorManagerListener;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import context.RuntimeContext;
import org.jetbrains.annotations.NotNull;
import ui.HistoryUi;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;


public class HistoryWin implements ToolWindowFactory {

    public static HistoryUi myToolWindow = new HistoryUi();



    public HistoryWin(){ }


    /**
     * 创建控件内容 2017/3/24 09:02
     * @param project 项目
     * @param toolWindow 窗口
     */
    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {


        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        JComponent jComponent = myToolWindow.getPanel1();


        Content content = contentFactory.createContent(jComponent, "", false);
        toolWindow.getContentManager().addContent(content);





//        project.getMessageBus().connect().subscribe(FileEditorManagerListener.FILE_EDITOR_MANAGER,new FileChangeListener());
//        EditorEventMulticaster eventMulticaster = EditorFactory.getInstance().getEventMulticaster();
//        SelectListener selectListener = new SelectListener();
//        eventMulticaster.addSelectionListener(selectListener);
//        eventMulticaster.addEditorMouseListener(selectListener);

    }



}
