package utils;

import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.util.PsiTreeUtil;

public class PsiUtils {

    public static PsiMethod[] getFileMethods(PsiElement element){
        return PsiTreeUtil.getChildrenOfType(element,PsiMethod.class);
    }


}
