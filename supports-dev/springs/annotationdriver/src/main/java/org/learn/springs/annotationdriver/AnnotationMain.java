package org.learn.springs.annotationdriver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@ComponentScan
public class AnnotationMain {

    public static void main(String[] args){

        AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
        ctx.register(AnnotationMain.class);
        ctx.refresh();


        Entitlement ent = (Entitlement)ctx.getBean("entitlement");
//        System.out.println(ent.getName());
//        System.out.println(ent.getTime());


        Entitlement ent2 = (Entitlement)ctx.getBean("entitlement2");
//        System.out.println(ent2.getName());
//        System.out.println(ent2.getTime());

        ctx.close();

    }
}
