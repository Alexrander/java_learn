package org.learn.springs.annotationdriver;

import org.learn.springs.annotationdriver.Entitlement;
import org.learn.springs.annotationdriver.annotation.AutoProxyTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


@Component
public class AppConfig {

    @Autowired
    ServiceTest serviceTest;


    @Bean(name="entitlement")
    public Entitlement entitlement() {
        Entitlement ent= new Entitlement();
        ent.setName("Entitlement");
        ent.setTime(1);
        return ent;
    }

    @Bean(name="entitlement2")
    public Entitlement entitlement2() {
        Entitlement ent= new Entitlement();
        ent.setName("Entitlement2");
        ent.setTime(2);
        serviceTest.testMethod();
        return ent;
    }



}
