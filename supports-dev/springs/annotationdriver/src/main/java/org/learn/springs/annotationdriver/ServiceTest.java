package org.learn.springs.annotationdriver;

import org.learn.springs.annotationdriver.annotation.AutoProxyTest;
import org.springframework.stereotype.Service;

@Service
public class ServiceTest {

    @AutoProxyTest
    void testMethod(){}
}
