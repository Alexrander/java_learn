package org.learn.springs.annotationdriver.annotation;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.learn.supports.spring.autoproxy.BeanAwareHookContext;
import org.learn.supports.spring.autoproxy.BeanAwareLifeCycleHook;
import org.springframework.context.annotation.Configuration;
import org.learn.develop.utils.AnnotationUtils;


@Configuration
public class ProxyConfig extends BeanAwareLifeCycleHook {


    protected boolean wrapIfNecessary(Object bean, String beanName, Object cacheKey, BeanAwareHookContext beanAwareHookContext) {
        System.err.println(bean.getClass().getName());
        if(AnnotationUtils.existsAnnotation(AutoProxyTest.class,bean.getClass())){
            System.err.println(bean.getClass().getName());
            beanAwareHookContext.setInterceptor(new MethodInterceptor() {
                public Object invoke(MethodInvocation invocation) throws Throwable {
                    System.err.println("i'm method invocation");
                    return invocation.proceed();
                }
            });
            return true;
        }
        return false;
    }



    public void destroy() throws Exception {

    }

    public void afterPropertiesSet() throws Exception {

    }
}
