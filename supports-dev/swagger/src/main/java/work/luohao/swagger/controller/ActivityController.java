//package work.luohao.swagger.controller;
//
//import com.biz.soa.kuaihe.vo.base.BootstrapTablePageResult;
//import com.biz.soa.kuaihe.vo.share.ActivityShareDetailReqVo;
//import com.biz.soa.kuaihe.vo.share.ActivityShareDetailResVo;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//import work.luohao.swagger.service.ActivityService;
//
//@RestController
//@RequestMapping("/activity")
//@Api("swsss")
//public class ActivityController {
//
//    @Autowired
//    ActivityService activityService;
//
//    /**
//     *
//     * todo F2 Controller 获取邀请明细
//     *
//     * @return
//     */
//    @RequestMapping(value = "/findShareDetail", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value="获取用户列表", notes="")
//    public BootstrapTablePageResult<ActivityShareDetailResVo> findShareDetail(ActivityShareDetailReqVo vo) {
//
//        return activityService.getShareRegister(vo);
//    }
//
//
//
//    @RequestMapping(value = "/userShare", method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value="获取用户列表", notes="")
//    public BootstrapTablePageResult<ActivityShareDetailResVo> userShare(ActivityShareDetailReqVo vo) {
//
//        return activityService.getShareRegister(vo);
//    }
//
//
//
//}
