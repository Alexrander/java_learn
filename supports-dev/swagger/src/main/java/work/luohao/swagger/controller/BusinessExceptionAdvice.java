package work.luohao.swagger.controller;


import cn.wine.common.exception.BusinessException;
import cn.wine.common.vo.JsonResult;
import com.biz.soa.exception.SoaException;
import com.biz.soa.service.ApiExceptions;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
@Slf4j
@Component
public class BusinessExceptionAdvice {

    @ExceptionHandler
    public JsonResult exceptionHandler(Exception ex) {
//        if (ex instanceof BusinessException) {
//            BusinessException businessException = (BusinessException) ex;
//            return new JsonResult(businessException.getCode(), businessException.getMessage());
//            return new
//        }

        if(ex instanceof SoaException){
            SoaException soaException = (SoaException) ex;
            return new JsonResult(soaException.getCode(), soaException.getMessage());
        }

        return new JsonResult(ApiExceptions.Global.SERVER_EXCEPTION.getCode(), ApiExceptions.Global.SERVER_EXCEPTION.getDescription());
    }

}
