//package work.luohao.swagger.controller;
//
//import com.biz.soa.kuaihe.vo.base.PageVo;
//import com.biz.soa.kuaihe.vo.sevencolor.CouponVoFor;
//import com.biz.soa.vo.PageResult;
//import com.biz.soa.vo.promotion.game.*;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.*;
//import org.springframework.web.multipart.MultipartFile;
//import work.luohao.swagger.vo.BootstrapTablePageResult;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.OutputStream;
//import java.net.URL;
//import java.net.URLEncoder;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//
//@RestController
//@RequestMapping("/game")
//@Api(tags = "游戏")
//public class GameController {
//
//    @ApiOperation(value = " 0-模板下载")
//    @RequestMapping(value = "/template", method = RequestMethod.GET)
//    public void template(HttpServletRequest request,HttpServletResponse response) throws Exception {
//
//    }
//
//
//    @ApiOperation(value = " 0-上传接口")
//    @RequestMapping(value = "/upload", method = RequestMethod.POST)
//    public Map<String, Object> uploadImg(MultipartFile file, HttpServletRequest request) {
//        return new HashMap<>();
//    }
//
//    @ApiOperation(value = " 0-下载接口")
//    @RequestMapping(value = "/download/{id}", method = RequestMethod.GET)
//    public void download(@PathVariable("id")String id )  { }
//
//
//    /********************************************************************************************************************
//     *  0-融合配置
//    ********************************************************************************************************************/
//
//    /**
//     * 读取配置
//     * @return
//     */
//    @RequestMapping(value = "getGlobalConfig", method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value = " 0-融合配置 获取配置")
//    public BaseConfigVo getGlobalConfig(){
//        return new BaseConfigVo();
//    }
//
//
//    /**
//     * 保存设置
//     * @param vo
//     * @return
//     */
//    @RequestMapping(value = "saveGlobalConfig", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "0-融合配置 保存配置")
//    public String saveGlobalConfig(@RequestBody BaseConfigVo vo) {
//        return "done";
//    }
//
//
//
//    /********************************************************************************************************************
//     *      1-题库设置
//    ********************************************************************************************************************/
//
//    /**
//     * 查询题库
//     * @return
//     * @throws IOException
//     */
//    @RequestMapping(value = "getSubjectByPage", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "   1-题库设置 查询题库")
//    public PageResult<SubjectVo> getSubjectByPage(@RequestBody PageVo pageRequest) throws IOException {
//        return new PageResult<SubjectVo>();
//    }
//
//    /**
//     * 保存题库
//     * @param vo
//     * @return
//     * @throws IOException
//     */
//    @RequestMapping(value = "saveSubject", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "   1-题库设置 保存题库设置")
//    public String saveSubject(@RequestBody SubjectVo vo) throws IOException {
//        return "done";
//    }
//    /**
//     * 删除题库
//     * @return
//     * @throws IOException
//     */
//    @RequestMapping(value = "delSubject/{id}", method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value = "  1-题库设置 删除题库")
//    public String delSubject(@PathVariable("id")String id) {
//
//        return "done";
//    }
//
//
//
//    /********************************************************************************************************************
//     *  2-兑换设置
//    ********************************************************************************************************************/
//
//    /**
//     * 优惠券列表
//     * @param pageRequest
//     * @return
//     */
//    @RequestMapping(value = "findCoupons", method = {RequestMethod.POST})
//    @ResponseBody
//    @ApiOperation(value = "2-兑换设置 优惠券列表")
//    public  BootstrapTablePageResult<CouponVoFor>  findCoupons(@RequestBody  PageVo pageRequest){
//        return new BootstrapTablePageResult<CouponVoFor>();
//    }
//
//    /**
//     * 获取奖品列表
//     * @param pageRequest
//     * @return
//     */
//    @RequestMapping(value = "getGameRewardByPage", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "2-兑换设置 获取奖品列表")
//    public PageResult<GameRewardVo> getGameRewardByPage(@RequestBody PageVo pageRequest){
//        return new PageResult<GameRewardVo>();
//    }
//
//
//    /**
//     * 保存奖品
//     * @param gameRewardVo
//     * @return
//     */
//    @RequestMapping(value = "saveGameReward", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "2-兑换设置 保存奖品")
//    public String saveGameReward(@RequestBody List<GameRewardVo> gameRewardVo){
//        return "done";
//    }
//
//
//    /**
//     * 删除奖品
//     * @param gameRewardId
//     * @return
//     */
//    @RequestMapping(value = "delGameReward/{gameRewardId}", method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value = "2-兑换设置 删除奖品")
//    public String delGameReward(@PathVariable("gameRewardId")String gameRewardId){
//        return "done";
//    }
//
//
//
//
//    /********************************************************************************************************************
//     *  3-分享设置
//    ********************************************************************************************************************/
//
//    /**
//     * 查询
//     * @return
//     */
//    @RequestMapping(value = "getGameShare", method = RequestMethod.GET)
//    @ResponseBody
//    @ApiOperation(value = "3-分享设置 查询分享设置")
//    public GameShareVo getGameShare(){
//        return  new GameShareVo();
//    }
//
//
//
//    /**
//     * 保存配置
//     * @param vo
//     * @return
//     */
//    @RequestMapping(value = "saveGameShare", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "3-分享设置 保存分享设置")
//    public GameShareVo saveGameShare(@RequestBody GameShareVo vo){
//        return  new GameShareVo();
//    }
//
//
//
//    /********************************************************************************************************************
//     *  4   - 酒量管理
//    ********************************************************************************************************************/
//
//    /**
//     * 酒量列表
//     * @param pageRequest
//     * @return
//     */
//    @RequestMapping(value = "findWineAmountList", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "4   - 酒量管理 酒量列表")
//    public BootstrapTablePageResult<GameUserStateVo> findWineAmountList(@RequestBody PageVo pageRequest) {
//        BootstrapTablePageResult<GameUserStateVo> page = new BootstrapTablePageResult<>();
//        return page;
//    }
//
//
//    /**
//     * 酒量兑换记录
//     * @param pageRequest
//     * @return
//     */
//    @RequestMapping(value = "findRewardLog", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value = "4   - 酒量管理 酒量兑换记录")
//    public BootstrapTablePageResult<GameRewardExchangeLogVo>  findRewardLog(@RequestBody PageVo pageRequest) {
//        BootstrapTablePageResult<GameRewardExchangeLogVo> page = new BootstrapTablePageResult<>();
//        return page;
//    }
//
//
//
//
//
//
//
//
//
//
//
//}
