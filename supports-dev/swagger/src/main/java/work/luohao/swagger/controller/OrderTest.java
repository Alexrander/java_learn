//package work.luohao.swagger.controller;
//
//
//
//
//import com.alibaba.dubbo.config.annotation.Reference;
//import com.biz.soa.exception.SoaException;
//import com.biz.soa.service.shopcart.DepotShopCartService;
//import com.biz.soa.vo.shopcart.SoaDepotShopCartRespVo;
//import com.biz.soa.vo.shopcart.supershopcart.req.SoaDepotShopCartReqVo;
//import io.swagger.annotations.Api;
//import io.swagger.annotations.ApiOperation;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.ResponseBody;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.math.BigDecimal;
//
//
//@RestController
//@RequestMapping("/order")
//@Api("order")
//public class OrderTest {
//
//    @Reference
//    private DepotShopCartService depotShopCartService;
//
//
//    /**
//     *
//     * todo F2 Controller 获取邀请明细
//     *
//     * @return
//     */
//    @RequestMapping(value = "/superShop", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value="superShop", notes="")
//    public SoaDepotShopCartRespVo superShop(){
//
//
//        SoaDepotShopCartReqVo soaDepotShopCartReqVo = new SoaDepotShopCartReqVo();
//        soaDepotShopCartReqVo.setUserId(2578827L);
//        soaDepotShopCartReqVo.setUserLevel(1);
//        soaDepotShopCartReqVo.setLat(new BigDecimal(30.680470607028482));
//        soaDepotShopCartReqVo.setLon(new BigDecimal(104.0685765314098));
//        soaDepotShopCartReqVo.setGeoId(75L);
//
//
//
//        SoaDepotShopCartRespVo soaDepotShopCartRespVo = null;
//        try {
//            soaDepotShopCartRespVo = depotShopCartService.findSuperShopCartProducts(soaDepotShopCartReqVo);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        soaDepotShopCartRespVo.setAmount(2);
//        return soaDepotShopCartRespVo;
//    }
//
//
//    @RequestMapping(value = "/findAll", method = RequestMethod.POST)
//    @ResponseBody
//    @ApiOperation(value="findAll", notes="")
//    public SoaDepotShopCartRespVo findAll() throws SoaException {
//
//        SoaDepotShopCartReqVo soaDepotShopCartReqVo = new SoaDepotShopCartReqVo();
////        soaDepotShopCartReqVo.setLocationDepotId(requestVo.getGlobalDepotId());
//        soaDepotShopCartReqVo.setGeoId(75L);
//        soaDepotShopCartReqVo.setLat(new BigDecimal(30.680470607028482));
//        soaDepotShopCartReqVo.setLon(new BigDecimal(104.0685765314098));
//        soaDepotShopCartReqVo.setUserId(2578827L);
//        soaDepotShopCartReqVo.setVersion("6.4.2");
//        soaDepotShopCartReqVo.setUserLevel(1);
//
//        return depotShopCartService.findSuperShopCartProducts(soaDepotShopCartReqVo);
//
//    }
//
//
//
//}
