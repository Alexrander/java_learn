package work.luohao.swagger.controller.gamefront;


import cn.wine.common.vo.JsonResult;
import com.biz.soa.exception.SoaException;
import com.biz.soa.vo.PageResult;
import com.biz.soa.vo.promotion.game.BaseConfigVo;
import com.biz.soa.vo.promotion.game.GameUserStateVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.learn.develop.utils.MobileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import work.luohao.swagger.controller.BaseController;
import work.luohao.swagger.service.game.GameConfigService;
import work.luohao.swagger.service.game.GameUserService;
import work.luohao.swagger.service.game.helps.GameDataTransfer;

@RestController
@RequestMapping("game/")
@Api(tags = "游戏首页[通过测试,字段有可能缺省]")
@Slf4j
public class GameIndexController  extends BaseController {

    @Autowired
    private GameUserService gameUserService;
    @Autowired
    private GameConfigService gameConfigService;



/********************************************************************************************************************
 *  全局配置
********************************************************************************************************************/
    @GetMapping("/config")
    @ApiOperation(value = "游戏配置[通过测试]", notes = "游戏配置")
    public JsonResult<BaseConfigVo> config(){
        return JsonResult.of(gameConfigService.getGameConfig());
    }


/********************************************************************************************************************
 *  用户信息
********************************************************************************************************************/
//not impl 需要重新调试
    @GetMapping("/sign")
    @ApiOperation(value = "签到[通过测试]", notes = "签到")
    public JsonResult<String> sign() throws SoaException {

        try {
            String msg = gameUserService.sign(this.getUserId());
            System.err.println(msg);
            System.err.println(2);
            return JsonResult.of(msg);
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }

    }


    @GetMapping("/userInfo")
    @ApiOperation(value = "用户游戏信息[通过测试]", notes = "用户游戏信息")
    public JsonResult<GameUserStateVo> userInfo() throws SoaException {
        try {

            GameUserStateVo gameUserStateVo = gameUserService.getUserStateVo(this.getUserId());
            gameUserStateVo.setUserName(MobileUtils.hidden(gameUserStateVo.getUserName()));

            BaseConfigVo baseConfigVo = gameConfigService.getGameConfig();


            String lv = GameDataTransfer.wineAmountToLv(gameUserStateVo.getWineAmount(),
                    baseConfigVo.getGameLv1(),
                    baseConfigVo.getGameLv2(),
                    baseConfigVo.getGameLv3(),
                    baseConfigVo.getGameLv4(),
                    baseConfigVo.getGameLv5());


            gameUserStateVo.setGameLv(lv.split("\\,")[0]);
            gameUserStateVo.setGameLvSerial(lv.split("\\,")[1]);

            gameUserStateVo.setNickName("等待实现");
            gameUserStateVo.setUserIcon("www.baidu.com");

            return JsonResult.of(gameUserStateVo);
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }



    }





    @GetMapping("/userRank")
    @ApiOperation(value = "用户游戏排行[通过测试]", notes = "用户游戏排行")
    public JsonResult<GameUserStateVo> userRank() throws SoaException {

        try {
            GameUserStateVo gameUserStateVo = gameUserService.getUserStateVo(this.getUserId(),true);

            gameUserStateVo.setUserName(MobileUtils.hidden(gameUserStateVo.getUserName()));
            gameUserStateVo.setNickName("等待实现");
            gameUserStateVo.setUserIcon("www.baidu.com");

            return JsonResult.of(gameUserStateVo);
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }



    }



    @GetMapping("/rank/{page}")
    @ApiOperation(value = "游戏排行[通过测试]", notes = "游戏排行")
    public JsonResult<PageResult<GameUserStateVo>> rank(@PathVariable("page") int page){
        page -= 1;
        PageResult<GameUserStateVo> pageResult = gameUserService.getUserRank(page);

        pageResult.getResult().forEach(x ->{
            try {
                x.setUserName(MobileUtils.hidden(x.getUserName()));
            }catch (Exception e){
                e.printStackTrace();
            }
            x.setNickName("等待实现");
            x.setUserIcon("www.baidu.com");
        });
        pageResult.setCurrentPage(page+1);
        pageResult.setHasNext(null);

        return JsonResult.of(pageResult);
    }





}
