package work.luohao.swagger.controller.gamefront;


import cn.wine.common.vo.JsonResult;
import com.alibaba.fastjson.JSON;
import com.biz.soa.exception.SoaException;
import com.biz.soa.service.promotion.backend.game.GamePageableRequestVo;
import com.biz.soa.vo.PageResult;
import com.biz.soa.vo.PageableRequestVo;
import com.biz.soa.vo.promotion.game.GameRewardExchangeLogVo;
import com.biz.soa.vo.promotion.game.GameRewardStatus;
import com.biz.soa.vo.promotion.game.GameRewardVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.codelogger.utils.MD5Utils;
import org.learn.develop.utils.MobileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import work.luohao.swagger.controller.BaseController;
import work.luohao.swagger.controller.gamefront.utils.TranslationUtil;
import work.luohao.swagger.service.game.GameRewardExchangeLogService;
import work.luohao.swagger.service.game.GameRewardService;

@RestController
@RequestMapping("game/reward/")
@Api(tags = "游戏兑换")
@Slf4j
public class GameRewardController extends BaseController {
    @Autowired
    GameRewardExchangeLogService gameRewardExchangeLogService;
    @Autowired
    GameRewardService gameRewardService;


    @GetMapping("/logs/{page}")
    @ApiOperation(value = "用户兑换记录[通过测试]", notes = "用户兑换记录")
    public JsonResult<PageResult<GameRewardExchangeLogVo>> logs(@PathVariable("page") int page){
        GamePageableRequestVo pageableRequestVo = new GamePageableRequestVo();
        pageableRequestVo.setPage(page-1);
        pageableRequestVo.setSize(10);
        pageableRequestVo.setSearchKey("userId");
        pageableRequestVo.setSearchValue(this.getUserId());
        PageResult<GameRewardExchangeLogVo> pageResult = gameRewardExchangeLogService.getPage(pageableRequestVo);
        pageResult.getResult().forEach(
                x->x.setUserName(MobileUtils.hidden(x.getUserName()))
        );
        return JsonResult.of(pageResult);
    }

    @GetMapping("/findRewards/{page}")
    @ApiOperation(value = "兑换列表[通过测试]", notes = "兑换列表")
    public JsonResult<Object> findRewards(@PathVariable("page") @ApiParam(value = "page",example = "123") int page){
        page -= 1;
        PageableRequestVo pageableRequestVo = new PageableRequestVo();
        pageableRequestVo.setPage(page);
        pageableRequestVo.setSize(10);
        pageableRequestVo.setSearchValue(this.getUserId());
        pageableRequestVo.setSearchKey("userId");

        PageResult<GameRewardVo> pageResult = gameRewardService.findRewardByPage(pageableRequestVo);

        pageResult.getResult().forEach(x->{
            x.setToken(rewardMd5(x));
        });


        return JsonResult.of(TranslationUtil.ObjFieldToString(pageResult,"id"));
    }




    @GetMapping("/exchange/{token}/{rewardId}")
    @ApiOperation(value = "进行兑换操作", notes = "进行兑换操作")
    public JsonResult<String> exchange(@PathVariable("token") @ApiParam(value = "token",example = "123")String token,
                                       @PathVariable("rewardId") @ApiParam(value = "rewardId",example = "123") String rewardId) throws SoaException {


        try {
            GameRewardVo gameRewardVo = gameRewardService.findRewardById(Long.valueOf(rewardId));


            if (!token.equals(rewardMd5(gameRewardVo)))
                throw new SoaException("数据已更改");

            gameRewardService.exchange(this.getUserId(),rewardId);
            return JsonResult.of(GameRewardStatus.SUCCESSED.getCode());
        }catch (Exception e){
            for (GameRewardStatus gameRewardStatus: GameRewardStatus.values()){
                if (e.getMessage().equals(gameRewardStatus.getCode()))
                    return JsonResult.of(gameRewardStatus.getCode());
            }
            throw  new SoaException(e.getMessage());
        }


    }


    //            酒量要管  优惠券ID要管 类型要管
    private String rewardMd5(GameRewardVo gameRewardVo){
        if (gameRewardVo ==null)
            return null;
        return  MD5Utils.getMD5(gameRewardVo.getAcquiredWineAmount()+gameRewardVo.getCouponId()+gameRewardVo.getType());
    }




}
