package work.luohao.swagger.controller.gamefront;


import cn.wine.common.vo.JsonResult;
import com.alibaba.fastjson.JSONObject;
import com.biz.soa.exception.SoaException;
import com.biz.soa.vo.promotion.game.GameResourceVo;
import com.biz.soa.vo.promotion.game.GameShareVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import work.luohao.swagger.controller.BaseController;
import work.luohao.swagger.controller.gamefront.utils.TranslationUtil;
import work.luohao.swagger.controller.gamefront.vo.GameResourceStringIdVO;
import work.luohao.swagger.service.game.GameRuntimeService;

@RestController
@RequestMapping("game/runtime/")
@Api(tags = "游戏运行相关")
public class GameRuntimeController extends BaseController {

    @Autowired
    GameRuntimeService gameRuntimeService;

    @GetMapping("/preStart")
    @ApiOperation(value = "游戏开始资格状态[通过测试]", notes = "游戏开始资格状态")
    public JsonResult<String> preStart() throws SoaException {
        try {
            return JsonResult.of(gameRuntimeService.prestart(this.getUserId()));
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }
    }



    @GetMapping("/start")
    @ApiOperation(value = "开始游戏 资源申请[通过测试]", notes = "开始游戏 资源申请")
    public JsonResult<GameResourceStringIdVO> start() throws SoaException {

        try {
            GameResourceVo gameResourceVo = gameRuntimeService.start(this.getUserId());

            GameResourceStringIdVO gameResourceStringIdVO =gameResourceVo.conventByCopyProperties(GameResourceStringIdVO.class);
            gameResourceStringIdVO.setToken(gameResourceVo.getToken()+"");

            return JsonResult.of(gameResourceStringIdVO);


        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }
    }

    @GetMapping("/end/{token}/{correct}")
    @ApiOperation(value = "清算游戏 V1", notes = "清算游戏 V1")
    public JsonResult<Object> end(@PathVariable("token") Long token, @PathVariable("correct") int correct) throws SoaException {
        try {
            return JsonResult.of(TranslationUtil.ObjFieldToString(gameRuntimeService.end(this.getUserId(),token,correct),"id"));
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }

    }



    @GetMapping("/shareInfo")
    @ApiOperation(value = "分享信息[通过测试]", notes = "分享信息")
    public JsonResult<GameShareVo> shareInfo() throws SoaException {

        try {
            return JsonResult.of(gameRuntimeService.shareInfo());
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }

    }

    @GetMapping("/doShare/{shareId}")
    @ApiOperation(value = "分享回调", notes = "分享回调")
    public JsonResult<String> doShare(@PathVariable("shareId") Long shareId) throws SoaException {
        System.err.println(shareId);

        try {
            return JsonResult.of(gameRuntimeService.doShare(Long.valueOf(this.getUserId())));
        }catch (Exception e){
            throw  new SoaException(e.getMessage());
        }


    }


}
