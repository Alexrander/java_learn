package work.luohao.swagger.controller.gamefront.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * 转译工具类
 * @author zhangning
 * @date 2016年9月30日
 * @reviewer 
 */
public class TranslationUtil {
	public static final String TRAN_ID = "id";

    public static <V> JSONObject ObjFieldToString(V suorce, String... fields){
		if(suorce == null){
			return null;
		}
		String jsonStr = JsonUtil.obj2Json(suorce);
		JSONObject json = JSONObject.parseObject(jsonStr);
		JSONObject jsonObject = jsonFieldToString(json,fields);
		return jsonObject;
	}
	
	public static <V> JSONArray listObjFieldToString(Collection<V> sources, String... fields){
		JSONArray jsonArray = new JSONArray();
		if(sources != null && sources.size() > 0){
			for (V source : sources) {
				String jsonStr = JsonUtil.obj2Json(source);
				JSONObject json = JSONObject.parseObject(jsonStr);
				JSONObject jsonObject = jsonFieldToString(json,fields);
				jsonArray.add(jsonObject);
			}
			
		}
		return jsonArray;
	}
	private static JSONArray jsonArrayFieldToString(JSONArray jsonArray, String[] fields){
		try {
			if(jsonArray != null && jsonArray.size() > 0){
				Iterator<Object> it = jsonArray.iterator();
				 while (it.hasNext()) {
					 Object nextObject = it.next();
					if(nextObject.getClass() == JSONObject.class){
						JSONObject jsonObject = (JSONObject) nextObject;
		                Set<String> keys = jsonObject.keySet();
		                if(keys != null && keys.size() > 0){
							for (String key : keys) {
								if(jsonObject.get(key) != null && jsonObject.get(key).getClass() == JSONArray.class){
									jsonObject.put(key, jsonArrayFieldToString(jsonObject.getJSONArray(key),fields));
								}else if(jsonObject.get(key) != null && jsonObject.get(key).getClass() == JSONObject.class){
									jsonObject.put(key,jsonFieldToString(jsonObject.getJSONObject(key),fields));
								}else{
									for (String field : fields) {
										if(key.equals(field)){
											Object object = jsonObject.get(field);
											if(object != null){
												jsonObject.put(field, object.toString());
											}
										}
									}
								}
							}
						}
					}
	            }
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonArray;
	}
	private static JSONObject jsonFieldToString(JSONObject jsonObject, String[] fields){
		try {
			if(jsonObject != null){
	            Set<String> keys = jsonObject.keySet();
	            if(keys != null && keys.size() > 0){
					for (String key : keys) {
						if(jsonObject.get(key) != null && jsonObject.get(key).getClass() == JSONArray.class){
							jsonObject.put(key, jsonArrayFieldToString(jsonObject.getJSONArray(key),fields));
						}else if(jsonObject.get(key) != null && jsonObject.get(key).getClass() == JSONObject.class){
							jsonObject.put(key,jsonFieldToString(jsonObject.getJSONObject(key),fields));
						}else{
							for (String field : fields) {
								if(key.equals(field)){
									Object object = jsonObject.get(field);
									if(object != null){
										jsonObject.put(field, object.toString());
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonObject;
	}
}
