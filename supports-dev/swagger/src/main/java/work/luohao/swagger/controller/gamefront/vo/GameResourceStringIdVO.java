package work.luohao.swagger.controller.gamefront.vo;

import com.biz.soa.util.Copyable;
import com.biz.soa.vo.promotion.game.GameRuntimeConfigVo;
import com.biz.soa.vo.promotion.game.GameSubjectItemVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;

@ApiModel(
    description = "游戏资源"
)
public class GameResourceStringIdVO implements Copyable {
    @ApiModelProperty("所有题目")
    private List<GameSubjectItemVo> gameSubjectItemVoList;
    @ApiModelProperty("运行时参数")
    private GameRuntimeConfigVo gameRuntimeConfigVo;
    @ApiModelProperty("游戏token")
    private String token;

    public GameResourceStringIdVO() {
    }

    public List<GameSubjectItemVo> getGameSubjectItemVoList() {
        return this.gameSubjectItemVoList;
    }

    public void setGameSubjectItemVoList(List<GameSubjectItemVo> gameSubjectItemVoList) {
        this.gameSubjectItemVoList = gameSubjectItemVoList;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public GameRuntimeConfigVo getGameRuntimeConfigVo() {
        return this.gameRuntimeConfigVo;
    }

    public void setGameRuntimeConfigVo(GameRuntimeConfigVo gameRuntimeConfigVo) {
        this.gameRuntimeConfigVo = gameRuntimeConfigVo;
    }
}
