package work.luohao.swagger.service;

import com.biz.soa.exception.SoaException;

public interface ITestService {


    public Object forTest() throws SoaException;
}
