package work.luohao.swagger.service.game;

import com.alibaba.dubbo.config.annotation.Reference;
import com.biz.soa.service.promotion.backend.game.GameBaseConfigDubboService;
import com.biz.soa.vo.promotion.game.BaseConfigVo;
import org.springframework.stereotype.Service;



@Service
public class GameConfigService  {
    @Reference
    private GameBaseConfigDubboService gameBaseConfigDubboService;


    public BaseConfigVo getGameConfig(){
        return gameBaseConfigDubboService.getCurrentConfig();
    }
}
