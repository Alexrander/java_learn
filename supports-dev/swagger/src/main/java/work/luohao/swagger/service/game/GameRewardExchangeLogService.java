package work.luohao.swagger.service.game;


import com.alibaba.dubbo.config.annotation.Reference;
import com.biz.soa.service.promotion.backend.game.GamePageableRequestVo;
import com.biz.soa.service.promotion.backend.game.GameRewardExchangeLogDubboService;
import com.biz.soa.vo.PageResult;
import com.biz.soa.vo.PageableRequestVo;
import com.biz.soa.vo.promotion.game.GameRewardExchangeLogVo;
import org.springframework.stereotype.Service;



@Service
public class GameRewardExchangeLogService  {
    @Reference
    private GameRewardExchangeLogDubboService gameRewardExchangeLogDubboService;




    public PageResult<GameRewardExchangeLogVo> getPage(GamePageableRequestVo var1){
        return gameRewardExchangeLogDubboService.getByPage(var1);
    }


}
