package work.luohao.swagger.service.game;

import com.alibaba.dubbo.config.annotation.Reference;
import com.biz.soa.service.promotion.backend.game.GameRewardDubboService;
import com.biz.soa.vo.PageResult;
import com.biz.soa.vo.PageableRequestVo;
import com.biz.soa.vo.promotion.game.GameRewardVo;
import org.springframework.stereotype.Service;

@Service
public class GameRewardService  {


    @Reference
    GameRewardDubboService dubboService;


    public PageResult<GameRewardVo> findRewardByPage(PageableRequestVo pageableRequestVo ){
        return  dubboService.findRewardByPage(pageableRequestVo);
    }


    public boolean exchange(String userId, String rewardId) throws Exception {
        return dubboService.exchangeReward(Long.valueOf(userId),Long.valueOf(rewardId));
    }


    public GameRewardVo findRewardById(Long rewardId) {
        return dubboService.findGameRewardById(rewardId);
    }
}
