package work.luohao.swagger.service.game;


import com.alibaba.dubbo.config.annotation.Reference;
import com.biz.soa.service.promotion.backend.game.GameRuntimeDubboService;
import com.biz.soa.service.promotion.backend.game.GameShareDubboService;
import com.biz.soa.vo.promotion.game.GameEndStateVO;
import com.biz.soa.vo.promotion.game.GameResourceVo;
import com.biz.soa.vo.promotion.game.GameShareVo;
import org.springframework.stereotype.Service;


@Service
public class GameRuntimeService  {

    @Reference(timeout = 5000)
    GameRuntimeDubboService runtimeDubboService;
    @Reference(timeout = 5000)
    GameShareDubboService gameShareDubboService;


    public String prestart(String userId) throws Exception {
        return runtimeDubboService.startCheck(Long.valueOf(userId));
    }


    public GameResourceVo start(String userId) throws Exception {
        return runtimeDubboService.start(Long.valueOf(userId));
    }

    public GameEndStateVO end(String userId, Long token, int correct) throws Exception {
        return runtimeDubboService.end(Long.valueOf(userId),token,correct);
    }


    public GameShareVo shareInfo() throws Exception {
        return gameShareDubboService.getGameShareAndInit();
    }


    public String doShare(Long userId) throws Exception {
        return null;
        //        return gameShareDubboService.doShare(userId);
    }
}

