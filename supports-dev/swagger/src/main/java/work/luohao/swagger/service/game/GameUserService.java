package work.luohao.swagger.service.game;

import com.alibaba.dubbo.config.annotation.Reference;
import com.biz.soa.service.promotion.backend.game.GamePageableRequestVo;
import com.biz.soa.service.promotion.backend.game.GameUserStateDubboService;
import com.biz.soa.vo.PageResult;

import com.biz.soa.vo.promotion.game.GameUserStateVo;
import org.springframework.stereotype.Service;



@Service
public class GameUserService {
    @Reference(timeout = 5000)
    private GameUserStateDubboService gameUserStateDubboService;

    public GameUserStateVo getUserStateVo(String userId) throws Exception {
        return gameUserStateDubboService.getGameUserStateAndInit(Long.valueOf(userId));
    }

    public GameUserStateVo getUserStateVo(String userId,boolean rankOrNot) throws Exception {
        return gameUserStateDubboService.getGameUserState(Long.valueOf(userId),rankOrNot);
    }


    public PageResult<GameUserStateVo> getUserRank(int page){
        return gameUserStateDubboService.getByPage(new GamePageableRequestVo(page,10));
    }

    public String sign(String userId) throws Exception {
        return gameUserStateDubboService.sign(Long.valueOf(userId));
    }


}
