package work.luohao.swagger.service.game.helps;

public class GameDataTransfer {

    public static String GAME_LV1 = "酒鬼,Lv1";
    public static String GAME_LV2 = "酒徒,Lv2";
    public static String GAME_LV3 = "酒圣,Lv3";
    public static String GAME_LV4 = "酒仙,Lv4";
    public static String GAME_LV5 = "酒神,Lv5";

    public static String wineAmountToLv(int wineAmount,String gameLv1,String gameLv2,String gameLv3,String gameLv4,String gameLv5){

        try {
            if(between(wineAmount,gameLv1))
                return GAME_LV1;
            if(between(wineAmount,gameLv2))
                return GAME_LV2;
            if(between(wineAmount,gameLv3))
                return GAME_LV3;
            if(between(wineAmount,gameLv4))
                return GAME_LV4;
            if(between(wineAmount,gameLv5))
                return GAME_LV5;

            return GAME_LV1;
        }catch (Exception e){
            return GAME_LV1;
        }
    }


    private static boolean between(int wineAmount,String gameLv){
        String[] sp = gameLv.split("-");
        int start = Integer.valueOf(sp[0]);
        int end;
        if (sp[1].trim().equalsIgnoreCase("inf")) {
            end = Integer.MAX_VALUE;
        }else {
            end = Integer.valueOf(sp[1]);
        }



        if (start<= wineAmount  && wineAmount <= end) {
            return true;
        }
        return false;
    }

    public static void main(String[] args){
        System.err.println("0-50".split("-")[0]);
        System.err.println("0-50".split("-")[1]);
    }
}
