package work.luohao.swagger.vo;

import com.bozhi.core.exception.ExceptionType;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 后台管理系统的json数据返回封装
 * @author maoyikun
 * @date 16-12-28
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ManageCommResult {

    private static final int CODE_SUCCESS = 0;

    //状态码，默认为0，成功返回0，其他异常情况自定义code的值
    private Integer code = CODE_SUCCESS;

    //返回的信息，如：操作失败，具体的失败的信息。
    private String msg;

    //请求之后返回的数据信息
    private Object data;


    public ManageCommResult() {}

    public ManageCommResult(Object data) {
        this(CODE_SUCCESS, null, data);
    }

    public ManageCommResult(String msg) {
        this(CODE_SUCCESS, msg, null);
    }

    public ManageCommResult(Integer code, String msg) {
        this(code, msg, null);
    }

    public ManageCommResult(String key, Object value) {
        this.code = 0;
        Map<String, Object> m = new HashMap<String, Object>(1);
        m.put(key, value);
        data = m;
    }

    public ManageCommResult(Integer code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }



    public Integer getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

    public Object getData() {
        return data;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
