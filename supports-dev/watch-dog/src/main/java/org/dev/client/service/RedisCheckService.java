package org.dev.client.service;

import org.dev.client.vo.Report;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;

@Service
public class RedisCheckService implements ServiceApi{

    @Override
    public Report ping(Report report) {
        try {
            Jedis jedis = new Jedis(report.getUrl(), report.getPort());
            String ping = jedis.ping();

            if (ping.equalsIgnoreCase("PONG")) {
                report.setAlive(true);
            }
        } catch (Exception e) {
            report.setAlive(false);
            report.setException(e);

        }
        return report;
    }


}
