package org.dev.client.service;

import org.dev.client.vo.Report;

import javax.validation.constraints.NotNull;


public interface ServiceApi {

    Report ping(@NotNull Report report);

}
