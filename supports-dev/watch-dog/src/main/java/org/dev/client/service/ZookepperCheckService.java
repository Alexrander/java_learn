package org.dev.client.service;

import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.dev.client.vo.Report;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;


@Service
public class ZookepperCheckService implements ServiceApi,Watcher {


    @Override
    public Report ping(@NotNull Report report) {
        try {
            ZooKeeper zookeeper = new ZooKeeper(report.getUrl(),report.getPort(),this);
            report.setAlive(zookeeper.getState() == ZooKeeper.States.CONNECTING);
            return report;
        }catch (Exception e ){
            report.setException(e);
            return report;
        }
    }



    @Override
    public void process(WatchedEvent watchedEvent) {}


    public static void main(String[] args){
        Report report = new Report();
        report.setUrl("192.168.10.9");
        report.setPort(2181);
        System.err.println(new ZookepperCheckService().ping(report).isAlive());
    }

}
