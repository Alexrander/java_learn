package org.dev.client.web;

import org.dev.client.service.RedisCheckService;
import org.dev.client.vo.Report;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis")
public class RedisCheckController {

    @Autowired
    private RedisCheckService redisCheckService;


    @GetMapping("/ping")
    public Report ping(){
        Report report = new Report();
        report.setUrl("192.168.20.5");
        report.setPort(6379);
        return redisCheckService.ping(report);
    }


}
