package org.share.supports.config;


public final class ConfigurationFactory {



    private static final String REGISTRY_CONF = DefaultConstants.REGISTRY_CONF;
    private static final String NAME_KEY = "name";
    private static final String FILE_TYPE = "file";


    public static final Configuration FILE_INSTANCE = new FileConfiguration(REGISTRY_CONF);

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static Configuration getInstance() {
        ConfigType configType ;
        String configTypeName = null;
        try {
            configTypeName = FILE_INSTANCE.getConfig(ConfigurationKeys.FILE_ROOT_CONFIG + ConfigurationKeys.FILE_CONFIG_SPLIT_CHAR
                    + ConfigurationKeys.FILE_ROOT_TYPE);
            configType = ConfigType.getType(configTypeName);
        } catch (Exception exx) {
            throw new RuntimeException("not support register type: " + configTypeName);
        }
        if (ConfigType.File == configType) {
            String pathDataId = ConfigurationKeys.FILE_ROOT_CONFIG + ConfigurationKeys.FILE_CONFIG_SPLIT_CHAR
                    + FILE_TYPE + ConfigurationKeys.FILE_CONFIG_SPLIT_CHAR
                    + NAME_KEY;
            String name = FILE_INSTANCE.getConfig(pathDataId);
            return new FileConfiguration(name);
        } else {
            System.err.println("not impl yet ");
            throw  new RuntimeException("not support yet: ");
//            return EnhancedServiceLoader.load(ConfigurationProvider.class, Objects.requireNonNull(configType).name()).provide();
        }
    }
}
