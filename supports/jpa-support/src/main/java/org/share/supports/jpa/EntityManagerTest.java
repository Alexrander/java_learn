package org.share.supports.jpa;

import org.share.supports.jpa.entity.GameReward;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import java.util.Arrays;

@SpringBootApplication
public class EntityManagerTest implements CommandLineRunner {


    public static void main(String[] args){
        SpringApplication.run(EntityManagerTest.class,args);
    }

    @Override
    public void run(String... args) throws Exception {
        EntityManager em = Persistence
                .createEntityManagerFactory("jpaUnit").createEntityManager();


        String[] data={"123","344","545"};
        GameReward gameReward = new GameReward();
        gameReward.setId(32433234324L);
        gameReward.setCouponId(1233L);
        gameReward.setType("213");
        gameReward.setCouponName(Arrays.asList(data));

        em.persist(gameReward);

//        em.flush();


    }
}
