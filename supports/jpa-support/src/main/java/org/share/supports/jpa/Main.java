package org.share.supports.jpa;

import org.share.supports.jpa.dao.GameRewardRepository;
import org.share.supports.jpa.entity.GameReward;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;

@SpringBootApplication
public class Main implements CommandLineRunner {

    @Autowired
    JdbcTemplate jdbcTemplate;
    @Autowired
    GameRewardRepository gameRewardRepository;

    public static void main(String[] args){
        SpringApplication.run(Main.class,args);
    }

    @Override
    @Transactional
    public void run(String... args) throws Exception {
        String[] data={"123","344","545"};
        GameReward gameReward = new GameReward();
        gameReward.setId(324234324L);
        gameReward.setCouponId(123L);
        gameReward.setType("213");
        gameReward.setCouponName(Arrays.asList(data));
        gameRewardRepository.save(gameReward);

//        System.err.println(jdbcTemplate.queryForList("select count(*) from prm_share_register_log;"));
    }
}
