package org.share.supports.jpa.dao;


import org.share.supports.jpa.entity.GameReward;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameRewardRepository extends JpaRepository<GameReward, Long> {


}
