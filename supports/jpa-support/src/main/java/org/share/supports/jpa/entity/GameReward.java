package org.share.supports.jpa.entity;



import lombok.Data;
import lombok.EqualsAndHashCode;
import org.share.supports.jpa.converter.ListToStringConverter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "prm_game_reward")
@Data
public class GameReward extends BaseEntity {

    @Column(columnDefinition = "varchar(200) comment '奖品类型' ")
    private String type;

    @Column(columnDefinition = "bigint comment '优惠券ID'  ")
    private Long couponId;

    @Column(columnDefinition = "varchar(200) comment '优惠券名字'    ")
    @Convert(converter = ListToStringConverter.class)
    private List<String> couponName;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public List<String> getCouponName() {
        return couponName;
    }

    public void setCouponName(List<String> couponName) {
        this.couponName = couponName;
    }
}
