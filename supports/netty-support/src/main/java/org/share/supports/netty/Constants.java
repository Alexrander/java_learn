package org.share.supports.netty;

import io.netty.util.NettyRuntime;

public class Constants {
    private  int bossThreadSize = 1;
    private  String bossThreadPrefix="NettyBoss";
    private  int workThreadSize = NettyRuntime.availableProcessors() * 2;
    private  String workThreadPrefix = "NettyServerNIOWorker";
    private String clientSelectorThreadPrefix = "NettyClientSelector";
    private  int selectorThreadSizeThreadSize = 1;

    private int listenPort = 9876;


    private int serverSocketSendBufSize = 153600;
    private int serverSocketResvBufSize = 153600;
    private int writeBufferHighWaterMark = 67108864;
    private int writeBufferLowWaterMark = 1048576;
    private int soBackLogSize = 1024;


    private int channelMaxReadIdleSeconds = 15;


    public int getBossThreadSize() {
        return bossThreadSize;
    }

    public void setBossThreadSize(int bossThreadSize) {
        this.bossThreadSize = bossThreadSize;
    }

    public String getBossThreadPrefix() {
        return bossThreadPrefix;
    }

    public void setBossThreadPrefix(String bossThreadPrefix) {
        this.bossThreadPrefix = bossThreadPrefix;
    }

    public int getWorkThreadSize() {
        return workThreadSize;
    }

    public void setWorkThreadSize(int workThreadSize) {
        this.workThreadSize = workThreadSize;
    }

    public String getWorkThreadPrefix() {
        return workThreadPrefix;
    }

    public void setWorkThreadPrefix(String workThreadPrefix) {
        this.workThreadPrefix = workThreadPrefix;
    }

    public int getListenPort() {
        return listenPort;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    public int getSoBackLogSize() {
        return soBackLogSize;
    }

    public int getServerSocketSendBufSize() {
        return serverSocketSendBufSize;
    }

    public void setServerSocketSendBufSize(int serverSocketSendBufSize) {
        this.serverSocketSendBufSize = serverSocketSendBufSize;
    }

    public int getServerSocketResvBufSize() {
        return serverSocketResvBufSize;
    }

    public void setServerSocketResvBufSize(int serverSocketResvBufSize) {
        this.serverSocketResvBufSize = serverSocketResvBufSize;
    }

    public void setSoBackLogSize(int soBackLogSize) {
        this.soBackLogSize = soBackLogSize;
    }

    public int getWriteBufferHighWaterMark() {
        return writeBufferHighWaterMark;
    }

    public void setWriteBufferHighWaterMark(int writeBufferHighWaterMark) {
        this.writeBufferHighWaterMark = writeBufferHighWaterMark;
    }

    public int getWriteBufferLowWaterMark() {
        return writeBufferLowWaterMark;
    }

    public void setWriteBufferLowWaterMark(int writeBufferLowWaterMark) {
        this.writeBufferLowWaterMark = writeBufferLowWaterMark;
    }

    public int getChannelMaxReadIdleSeconds() {
        return channelMaxReadIdleSeconds;
    }

    public int getSelectorThreadSizeThreadSize() {
        return selectorThreadSizeThreadSize;
    }

    public void setSelectorThreadSizeThreadSize(int selectorThreadSizeThreadSize) {
        this.selectorThreadSizeThreadSize = selectorThreadSizeThreadSize;
    }

    public void setChannelMaxReadIdleSeconds(int channelMaxReadIdleSeconds) {
        this.channelMaxReadIdleSeconds = channelMaxReadIdleSeconds;

    }

    public String getClientSelectorThreadPrefix() {
        return clientSelectorThreadPrefix;
    }

    public void setClientSelectorThreadPrefix(String clientSelectorThreadPrefix) {
        this.clientSelectorThreadPrefix = clientSelectorThreadPrefix;
    }
}
