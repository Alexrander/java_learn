package org.share.supports.netty.events;

import io.netty.channel.socket.SocketChannel;

public interface  BootstrapHook {
      void staring();

      void socketChannelProcess(SocketChannel socketChannel);

      void started();


}
