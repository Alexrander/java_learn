package org.share.supports.netty.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.learn.develop.logger.Logger;
import org.learn.develop.logger.LoggerFactory;


public class EchoHandle extends ChannelInboundHandlerAdapter {

    Logger logger = LoggerFactory.getLogger(EchoHandle.class);

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
            logger.info(msg.toString());
            ctx.fireChannelRead(msg);
    }
    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        cause.printStackTrace();
        ctx.close();
    }
}
