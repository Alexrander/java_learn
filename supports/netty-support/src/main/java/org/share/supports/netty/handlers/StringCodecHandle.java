package org.share.supports.netty.handlers;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import org.learn.develop.logger.Logger;
import org.learn.develop.logger.LoggerFactory;

import java.nio.charset.Charset;
import java.util.List;

public class StringCodecHandle extends ByteCodecHandle<String> {
    Logger logger = LoggerFactory.getLogger(StringCodecHandle.class);

    @Override
    protected void encode(ChannelHandlerContext ctx, String msg, ByteBuf out) throws Exception {
        logger.info("StringCodecHandle encode[Out] ");
        out.writeBytes(msg.getBytes(Charset.forName("UTF-8")));
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        logger.info("StringCodecHandle decode[In] ");
        String str = in.readBytes(in.readableBytes()).toString(Charset.forName("UTF-8"));
        out.add(str);
    }
}
