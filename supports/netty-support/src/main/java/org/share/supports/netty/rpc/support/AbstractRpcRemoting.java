package org.share.supports.netty.rpc.support;

import io.netty.channel.Channel;

public class AbstractRpcRemoting {


    protected void sendMessage(Channel channel, Object msg) {
        channel.writeAndFlush(msg);
    }

    protected void sendAsyncMessage(Channel channel, Object msg) {
        channel.writeAndFlush(msg);
    }



}
