package org.learn.supports.spring.advice;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

/**
 * @Author liubin
 * @Date 2019-05-23
 * @Description 异常处理
 **/
@RestControllerAdvice
@Slf4j
public class BusinessExceptionAdvice  implements ResponseBodyAdvice {

    @ExceptionHandler
    public JsonResult exceptionHandler(Exception ex) {
        ex.printStackTrace();
        return new JsonResult();
    }

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        return false;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType,
                                  MediaType selectedContentType, Class selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {

        return body;
    }
}
