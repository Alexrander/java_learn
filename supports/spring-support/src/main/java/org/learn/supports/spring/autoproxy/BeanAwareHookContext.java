package org.learn.supports.spring.autoproxy;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.context.ApplicationContext;

import java.util.HashSet;
import java.util.Set;

public class BeanAwareHookContext {
    private ApplicationContext applicationContext;
    private MethodInterceptor interceptor;
    private static final Set<String> PROXYED_SET = new HashSet<>();


    public ApplicationContext getApplicationContext() {
        return applicationContext;
    }

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    public MethodInterceptor getInterceptor() {
        return interceptor;
    }

    public void setInterceptor(MethodInterceptor interceptor) {
        this.interceptor = interceptor;
    }

    public static Set<String> getProxyedSet() {
        return PROXYED_SET;
    }
}
