package org.learn.supports.spring.tx;

import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

/**
 *{@link org.springframework.transaction.support.TransactionSynchronizationAdapter} 周期
 *{@link org.springframework.transaction.support.TransactionSynchronizationManager} 管理器官
 */
public abstract class TransactionLifeCycleHook {

    static void afterCommit(Runnable runnable){
        assert TransactionSynchronizationManager.isActualTransactionActive();
        TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter(){
            @Override
            public void afterCommit(){runnable.run();}
        });
    }

}
