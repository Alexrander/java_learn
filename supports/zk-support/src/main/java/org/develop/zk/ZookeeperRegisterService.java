//package org.develop.zk;
//
//import org.I0Itec.zkclient.IZkChildListener;
//import org.I0Itec.zkclient.IZkStateListener;
//import org.I0Itec.zkclient.ZkClient;
//import org.apache.zookeeper.Watcher;
//import org.develop.register.RegistryService;
//import org.learn.develop.utils.CollectionUtils;
//
//import java.net.InetSocketAddress;
//import java.util.*;
//import java.util.concurrent.ConcurrentHashMap;
//import java.util.concurrent.ConcurrentMap;
//
//public class ZookeeperRegisterService   implements RegistryService<IZkChildListener> {
//
//    private static final String ROOT_PATH_WITHOUT_SUFFIX = DefaultConstants.ROOT_PATH_WITHOUT_SUFFIX;
//
//
//
//    private static volatile ZookeeperRegisterService instance;
//    private static volatile ZkClient zkClient;
//    private static final Set<String> REGISTERED_PATH_SET = Collections.synchronizedSet(new HashSet<>(1));
//    private static final ConcurrentMap<String, List<IZkChildListener>> LISTENER_SERVICE_MAP = new ConcurrentHashMap<>();
//
//
//
//
//
//    static ZookeeperRegisterService getInstance() {
//        if (null == instance) {
//            synchronized (ZookeeperRegisterService.class) {
//                if (null == instance) {
//                    instance = new ZookeeperRegisterService();
//                }
//            }
//        }
//        return instance;
//    }
//
//    private ZookeeperRegisterService() { }
//
//
//    @Override
//    public void register(InetSocketAddress address) throws Exception {
//
//    }
//
//    @Override
//    public void unregister(InetSocketAddress address) throws Exception {
//
//    }
//
//    @Override
//    public void subscribe(String cluster, IZkChildListener listener) throws Exception {
//
//    }
//
//    @Override
//    public void unsubscribe(String cluster, IZkChildListener listener) throws Exception {
//
//    }
//
//    @Override
//    public List<InetSocketAddress> lookup(String key) throws Exception {
//        return null;
//    }
//
//    @Override
//    public void close() throws Exception {
//
//    }
//
//
//    // visible for test.
//    ZkClient buildZkClient(String address, int sessionTimeout, int connectTimeout) {
//        ZkClient zkClient = new ZkClient(address, sessionTimeout,connectTimeout);
//        if (!zkClient.exists(ROOT_PATH_WITHOUT_SUFFIX)) {
//            zkClient.createPersistent(ROOT_PATH_WITHOUT_SUFFIX, true);
//        }
//        zkClient.subscribeStateChanges(new IZkStateListener() {
//
//            @Override
//            public void handleStateChanged(Watcher.Event.KeeperState keeperState) throws Exception {
//                //ignore
//            }
//
//            @Override
//            public void handleNewSession() throws Exception {
//                recover();
//            }
//
//            @Override
//            public void handleSessionEstablishmentError(Throwable throwable) throws Exception {
//                //ignore
//            }
//        });
//        return zkClient;
//    }
//
//    private void recover() throws Exception {
//        // recover Server
//        if (!REGISTERED_PATH_SET.isEmpty()) {
//            REGISTERED_PATH_SET.forEach(path -> doRegister(path));
//        }
//        // recover client
//        if (!LISTENER_SERVICE_MAP.isEmpty()) {
//            Map<String, List<IZkChildListener>> listenerMap = new HashMap<>(LISTENER_SERVICE_MAP);
//            for (Map.Entry<String, List<IZkChildListener>> listenerEntry : listenerMap.entrySet()) {
//                List<IZkChildListener> iZkChildListeners = listenerEntry.getValue();
//                if (CollectionUtils.isEmpty(iZkChildListeners)) {
//                    continue;
//                }
//                for (IZkChildListener listener : iZkChildListeners) {
//                    subscribe(listenerEntry.getKey(), listener);
//                }
//            }
//        }
//    }
//
//    private ZkClient getClientInstance() {
//        if (zkClient == null) {
//            synchronized (ZookeeperRegisterService.class) {
//                if (null == zkClient) {
//                    zkClient = buildZkClient(FILE_CONFIG.getConfig(FILE_CONFIG_KEY_PREFIX + SERVER_ADDR_KEY),
//                            FILE_CONFIG.getInt(FILE_CONFIG_KEY_PREFIX + SESSION_TIME_OUT_KEY),
//                            FILE_CONFIG.getInt(FILE_CONFIG_KEY_PREFIX + CONNECT_TIME_OUT_KEY));
//                }
//            }
//        }
//        return zkClient;
//    }
//
//    private boolean doRegister(String path) {
//        if (checkExists(path)) {
//            return false;
//        }
//        createParentIfNotPresent(path);
//        getClientInstance().createEphemeral(path, true);
//        REGISTERED_PATH_SET.add(path);
//        return true;
//    }
//}
