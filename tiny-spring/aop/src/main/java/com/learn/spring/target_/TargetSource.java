package com.learn.spring.target_;

import com.learn.spring.lang.Nullable;

public interface TargetSource extends TargetClassAware {

    /**
     * Return the type of targets returned by this {@link TargetSource}.
     * <p>Can return {@code null}, although certain usages of a {@code TargetSource}
     * might just work with a predetermined target_ class.
     * @return the type of targets returned by this {@link TargetSource}
     */
    @Override
    @Nullable
    Class<?> getTargetClass();

    /**
     * Will all calls to {@link #getTarget()} return the same object?
     * <p>In that case, there will be no need to invoke {@link #releaseTarget(Object)},
     * and the AOP framework can cache the return value of {@link #getTarget()}.
     * @return {@code true} if the target_ is immutable
     * @see #getTarget
     */
    boolean isStatic();

    /**
     * Return a target_ instance. Invoked immediately before the
     * AOP framework calls the "target_" of an AOP method invocation.
     * @return the target_ object which contains the joinpoint,
     * or {@code null} if there is no actual target_ instance
     * @throws Exception if the target_ object can't be resolved
     */
    @Nullable
    Object getTarget() throws Exception;

    /**
     * Release the given target_ object obtained from the
     * {@link #getTarget()} method, if any.
     * @param target object obtained from a call to {@link #getTarget()}
     * @throws Exception if the object can't be released
     */
    void releaseTarget(Object target) throws Exception;
}