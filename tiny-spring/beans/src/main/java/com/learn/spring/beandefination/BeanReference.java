package com.learn.spring.beandefination;

public interface BeanReference  extends BeanMetadataElement {
    String getBeanName();

}
