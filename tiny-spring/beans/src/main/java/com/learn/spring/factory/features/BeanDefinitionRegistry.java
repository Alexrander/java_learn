package com.learn.spring.factory.features;


import com.learn.spring.beandefination.BeanDefinition;
import com.learn.spring.exceptions.BeanDefinitionStoreException;
import com.learn.spring.exceptions.NoSuchBeanDefinitionException;

public interface BeanDefinitionRegistry extends AliasRegistry{




    void registerBeanDefinition(String beanName, BeanDefinition beanDefinition)
            throws BeanDefinitionStoreException;
    void removeBeanDefinition(String beanName) throws NoSuchBeanDefinitionException;
    BeanDefinition getBeanDefinition(String beanName) throws NoSuchBeanDefinitionException;
    boolean containsBeanDefinition(String beanName);
    String[] getBeanDefinitionNames();
    int getBeanDefinitionCount();
    boolean isBeanNameInUse(String beanName);

}
