

package com.learn.spring.namespace;


import com.learn.spring.lang.Nullable;



@FunctionalInterface
public interface NamespaceHandlerResolver {
	@Nullable
	NamespaceHandler resolve(String namespaceUri);

}
