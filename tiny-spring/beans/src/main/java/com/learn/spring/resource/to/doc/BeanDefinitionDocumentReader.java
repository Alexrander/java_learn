
package com.learn.spring.resource.to.doc;

import com.learn.spring.exceptions.BeanDefinitionStoreException;
import com.learn.spring.resource.to.doc.vo.XmlReaderContext;
import org.w3c.dom.Document;


public interface BeanDefinitionDocumentReader {

	void registerBeanDefinitions(Document doc, XmlReaderContext readerContext) throws BeanDefinitionStoreException;


}
