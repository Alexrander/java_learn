package com.learn.spring.resource.to.doc;

import com.learn.spring.beangenerator.BeanNameGenerator;
import com.learn.spring.resource.Resource;
import com.learn.spring.resource.loader.ResourceLoader;
import com.learn.spring.exceptions.BeanDefinitionStoreException;
import com.learn.spring.factory.features.BeanDefinitionRegistry;
import com.learn.spring.lang.Nullable;

public interface BeanDefinitionReader {

    /******************************************************************************
     * Core
    ******************************************************************************/
    /**
     * Load bean definitions from the specified resource.
     * @param resource the resource unit
     * @return the number of bean definitions found
     * @throws BeanDefinitionStoreException in case of loading or parsing errors
     */
    int loadBeanDefinitions(Resource resource) throws BeanDefinitionStoreException;

    /**
     * Load bean definitions from the specified resources.
     * @param resources the resource descriptors
     * @return the number of bean definitions found
     * @throws BeanDefinitionStoreException in case of loading or parsing errors
     */
    int loadBeanDefinitions(Resource... resources) throws BeanDefinitionStoreException;

    /**
     * Load bean definitions from the specified resource location.
     * <p>The location can also be a location pattern, provided that the
     * ResourceLoader of this bean definition reader is a ResourcePatternResolver.
     * @param location the resource location, to be loaded with the ResourceLoader
     * (or ResourcePatternResolver) of this bean definition reader
     * @return the number of bean definitions found
     * @throws BeanDefinitionStoreException in case of loading or parsing errors
     * @see #getResourceLoader()
     * @see #loadBeanDefinitions(Resource)
     * @see #loadBeanDefinitions(Resource[])
     */
    int loadBeanDefinitions(String location) throws BeanDefinitionStoreException;

    /**
     * Load bean definitions from the specified resource locations.
     * @param locations the resource locations, to be loaded with the ResourceLoader
     * (or ResourcePatternResolver) of this bean definition reader
     * @return the number of bean definitions found
     * @throws BeanDefinitionStoreException in case of loading or parsing errors
     */
    int loadBeanDefinitions(String... locations) throws BeanDefinitionStoreException;



    /******************************************************************************
     * Helper
    ******************************************************************************/

    /**
     * Return the BeanNameGenerator to use for anonymous beans
     * (without explicit bean name specified).
     */
    BeanNameGenerator getBeanNameGenerator();



    /******************************************************************************
     * Getter and Setter
    ******************************************************************************/
    BeanDefinitionRegistry getRegistry();

    @Nullable
    ResourceLoader getResourceLoader();

    @Nullable
    ClassLoader getBeanClassLoader();


}
