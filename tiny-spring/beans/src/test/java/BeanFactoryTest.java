import com.learn.spring.OutputServiceImpl;
import com.learn.spring.factory.DefaultListableBeanFactory;
import com.learn.spring.resource.to.doc.XmlBeanDefinitionReader;
import org.junit.Test;

/**
 * @author yihua.huang@dianping.com
 */
public class BeanFactoryTest {

    static  DefaultListableBeanFactory defaultListableBeanFactory= new DefaultListableBeanFactory();
    static{
        XmlBeanDefinitionReader xmlBeanDefinitionReader = new XmlBeanDefinitionReader(defaultListableBeanFactory);
        xmlBeanDefinitionReader.loadBeanDefinitions("setter.xml");
    }



    @Test
    public void testLazy() throws Exception {

        OutputServiceImpl helloWorldService = (OutputServiceImpl) defaultListableBeanFactory.getBean("outputService");
        System.err.println(helloWorldService.toString());
        helloWorldService = (OutputServiceImpl) defaultListableBeanFactory.getBean("outputService");
        System.err.println(helloWorldService.toString());



        // 3.获取bean
//        HelloWorldService helloWorldService = (HelloWorldService) defaultListableBeanFactory.getBean("outputService");
//        helloWorldService.helloWorld();
    }

}
