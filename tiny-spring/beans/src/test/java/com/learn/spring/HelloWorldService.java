package com.learn.spring;

/**
 * @author yihua.huang@dianping.com
 */
public interface HelloWorldService {

    void helloWorld();
}
