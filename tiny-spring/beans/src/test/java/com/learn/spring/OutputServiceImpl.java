package com.learn.spring;

/**
 * @author yihua.huang@dianping.com
 */
public class OutputServiceImpl implements OutputService {
    private String userName;

    public OutputServiceImpl(){
        System.err.println("init");
    }
    @Override
    public void output(String text){
        System.out.println(text);
    }
    public String toString(){
        return this.userName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
