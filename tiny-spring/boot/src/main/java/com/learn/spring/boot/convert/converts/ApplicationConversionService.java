package com.learn.spring.boot.convert.converts;

import com.learn.spring.convert.ConversionService;
import com.learn.spring.convert.ConverterRegistry;
import com.learn.spring.convert.impl.DefaultConversionService;
import com.learn.spring.factory.resolver.StringValueResolver;
import com.learn.spring.format.DefaultFormattingConversionService;
import com.learn.spring.format.FormatterRegistry;
import com.learn.spring.format.FormattingConversionService;

/********************************************************************************************************************
 * OK
********************************************************************************************************************/
public class ApplicationConversionService extends FormattingConversionService {

    private static volatile ApplicationConversionService sharedInstance;

    public ApplicationConversionService() {
        this(null);
    }

    public ApplicationConversionService(StringValueResolver embeddedValueResolver) {
        if (embeddedValueResolver != null) {
            setEmbeddedValueResolver(embeddedValueResolver);
        }
        configure(this);
    }

    /**
     * Configure the given {@link FormatterRegistry} with formatters and converters
     * appropriate for most Spring Boot applications.
     * @param registry the registry of converters to add to (must also be castable to
     * ConversionService, e.g. being a {@link ConfigurableConversionService})
     * @throws ClassCastException if the given FormatterRegistry could not be cast to a
     * ConversionService
     */
    public static void configure(FormatterRegistry registry) {
        DefaultConversionService.addDefaultConverters(registry);
        DefaultFormattingConversionService.addDefaultFormatters(registry);
        addApplicationFormatters(registry);
        addApplicationConverters(registry);
    }


    /**
     * Add formatters useful for most Spring Boot applications.
     * @param registry the dubbo to register default formatters with
     */
    public static void addApplicationFormatters(FormatterRegistry registry) {
        registry.addFormatter(new CharArrayFormatter());
        registry.addFormatter(new InetAddressFormatter());
        registry.addFormatter(new IsoOffsetFormatter());
    }
    /**
     * Add converters useful for most Spring Boot applications.
     * @param registry the registry of converters to add to (must also be castable to
     * ConversionService, e.g. being a {@link ConfigurableConversionService})
     * @throws ClassCastException if the given ConverterRegistry could not be cast to a
     * ConversionService
     */
    public static void addApplicationConverters(ConverterRegistry registry) {
        addDelimitedStringConverters(registry);
        registry.addConverter(new StringToDurationConverter());
        registry.addConverter(new DurationToStringConverter());
        registry.addConverter(new NumberToDurationConverter());
        registry.addConverter(new DurationToNumberConverter());
        registry.addConverter(new StringToDataSizeConverter());
        registry.addConverter(new NumberToDataSizeConverter());
        registry.addConverterFactory(new StringToEnumIgnoringCaseConverterFactory());
    }

    /**
     * Add converters to support delimited strings.
     * @param registry the registry of converters to add to (must also be castable to
     * ConversionService, e.g. being a {@link ConfigurableConversionService})
     * @throws ClassCastException if the given ConverterRegistry could not be cast to a
     * ConversionService
     */
    public static void addDelimitedStringConverters(ConverterRegistry registry) {
        ConversionService service = (ConversionService) registry;
        registry.addConverter(new ArrayToDelimitedStringConverter(service));
        registry.addConverter(new CollectionToDelimitedStringConverter(service));
        registry.addConverter(new DelimitedStringToArrayConverter(service));
        registry.addConverter(new DelimitedStringToCollectionConverter(service));
    }

    /**
     * Return a shared default application {@code ConversionService} instance, lazily
     * building it once needed.
     * <p>
     * Note: This method actually returns an {@link ApplicationConversionService}
     * instance. However, the {@code ConversionService} signature has been preserved for
     * binary compatibility.
     * @return the shared {@code ApplicationConversionService} instance (never
     * {@code null})
     */
    public static ConversionService getSharedInstance() {
        ApplicationConversionService sharedInstance = ApplicationConversionService.sharedInstance;
        if (sharedInstance == null) {
            synchronized (ApplicationConversionService.class) {
                sharedInstance = ApplicationConversionService.sharedInstance;
                if (sharedInstance == null) {
                    sharedInstance = new ApplicationConversionService();
                    ApplicationConversionService.sharedInstance = sharedInstance;
                }
            }
        }
        return sharedInstance;
    }

}