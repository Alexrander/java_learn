/*
 * Copyright 2012-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.learn.spring.boot.properties;


import com.learn.spring.annnotation.BeanFactoryAnnotationUtils;
import com.learn.spring.boot.convert.converts.ApplicationConversionService;
import com.learn.spring.context.ApplicationContext;
import com.learn.spring.context.ConfigurableApplicationContext;
import com.learn.spring.convert.ConversionService;
import com.learn.spring.convert.Converter;
import com.learn.spring.convert.GenericConverter;
import com.learn.spring.exceptions.NoSuchBeanDefinitionException;
import com.learn.spring.factory.BeanFactory;
import com.learn.spring.factory.ListableBeanFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Utility to deduce the {@link ConversionService} to use for configuration properties
 * binding.
 *
 * @author Phillip Webb
 */
class ConversionServiceDeducer {

	private final ApplicationContext applicationContext;

	ConversionServiceDeducer(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

	public ConversionService getConversionService() {
		try {
			return this.applicationContext.getBean(
					ConfigurableApplicationContext.CONVERSION_SERVICE_BEAN_NAME,
					ConversionService.class);
		}
		catch (NoSuchBeanDefinitionException ex) {
			return new Factory(this.applicationContext.getAutowireCapableBeanFactory())
					.create();
		}
	}

	private static class Factory {

		@SuppressWarnings("rawtypes")
		private final List<Converter> converters;

		private final List<GenericConverter> genericConverters;

		Factory(BeanFactory beanFactory) {
			this.converters = beans(beanFactory, Converter.class,
					ConfigurationPropertiesBinding.VALUE);
			this.genericConverters = beans(beanFactory, GenericConverter.class,
					ConfigurationPropertiesBinding.VALUE);
		}

		private <T> List<T> beans(BeanFactory beanFactory, Class<T> type,
				String qualifier) {
			if (beanFactory instanceof ListableBeanFactory) {
				return beans(type, qualifier, (ListableBeanFactory) beanFactory);
			}
			return Collections.emptyList();
		}

		private <T> List<T> beans(Class<T> type, String qualifier,
				ListableBeanFactory beanFactory) {
			return new ArrayList<T>(BeanFactoryAnnotationUtils
					.qualifiedBeansOfType(beanFactory, type, qualifier).values());
		}

		public ConversionService create() {
			if (this.converters.isEmpty() && this.genericConverters.isEmpty()) {
				return ApplicationConversionService.getSharedInstance();
			}
			ApplicationConversionService conversionService = new ApplicationConversionService();
			for (Converter<?, ?> converter : this.converters) {
				conversionService.addConverter(converter);
			}
			for (GenericConverter genericConverter : this.genericConverters) {
				conversionService.addConverter(genericConverter);
			}
			return conversionService;
		}

	}

}
