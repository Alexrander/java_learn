package com.learn.spring.boot.web;

import com.learn.spring.env.AbstractEnvironment;
import com.learn.spring.env.StandardEnvironment;

public class StandardReactiveWebEnvironment extends StandardEnvironment {
    public StandardReactiveWebEnvironment(){
        System.err.println("error no StandardReactiveWebEnvironment module");
    }
}
