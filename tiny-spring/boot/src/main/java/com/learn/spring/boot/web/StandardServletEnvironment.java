package com.learn.spring.boot.web;


import com.learn.spring.env.StandardEnvironment;

public class StandardServletEnvironment extends StandardEnvironment {
    public static String SERVLET_CONTEXT_PROPERTY_SOURCE_NAME="no";
    public static String SERVLET_CONFIG_PROPERTY_SOURCE_NAME="no";
    public static String JNDI_PROPERTY_SOURCE_NAME="no";
    public StandardServletEnvironment(){
        System.err.println("error no StandardServletEnvironment module");
    }
}
