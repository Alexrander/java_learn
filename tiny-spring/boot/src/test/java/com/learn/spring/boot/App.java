package com.learn.spring.boot;

import com.learn.spring.annotation.*;
import com.learn.spring.boot.convert.converts.ApplicationConversionService;
import com.learn.spring.context.AnnotationConfigApplicationContext;
import com.learn.spring.context.ApplicationContext;
import com.learn.spring.context.ConfigurableApplicationContext;
import com.learn.spring.convert.ConversionService;
import com.learn.spring.env.ConfigurableEnvironment;
import com.learn.spring.env.StandardEnvironment;
import com.learn.spring.util.BeanUtils;
import org.junit.Test;




//@Component
@Configuration
public class App {

    @Bean
    public String okk(){return "vvvvvv";}

    public static void main(String[] args){

//        System.out.println("bean:"+SpringApplication.run(App.class));
        System.out.println("bean:"+SpringApplication.run(App.class).getBean("okk"));


    }

}
