/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.learn.spring.event;

import com.learn.spring.exprssion.AnnotatedElementKey;
import com.learn.spring.factory.BeanFactory;
import com.learn.spring.lang.Nullable;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Utility class handling the SpEL expression parsing. Meant to be used
 * as a reusable, thread-safe component.
 *
 * @author Stephane Nicoll
 * @since 4.2
 * @see CachedExpressionEvaluator
 */
public class EventExpressionEvaluator  {

//	private final Map<ExpressionKey, Expression> conditionCache = new ConcurrentHashMap<>(64);


	/**
	 * Specify if the condition defined by the specified expression matches.
	 */
	public boolean condition(String conditionExpression, ApplicationEvent event, Method targetMethod,
							 AnnotatedElementKey methodKey, Object[] args, @Nullable BeanFactory beanFactory) {
//
//		EventExpressionRootObject root = new EventExpressionRootObject(event, args);
//		MethodBasedEvaluationContext evaluationContext = new MethodBasedEvaluationContext(
//				root, targetMethod, args, getParameterNameDiscoverer());
//		if (beanFactory != null) {
//			evaluationContext.setBeanResolver(new BeanFactoryResolver(beanFactory));
//		}
//
//		return (Boolean.TRUE.equals(getExpression(this.conditionCache, methodKey, conditionExpression).getValue(
//				evaluationContext, Boolean.class)));
		System.err.println("block module #36");
		return false;
	}

}
