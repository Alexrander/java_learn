/*
 * Copyright 2002-2018 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.learn.spring.namespace.tag.parser;


import com.learn.spring.ParserContext;
import com.learn.spring.beandefination.AbstractBeanDefinition;
import com.learn.spring.lang.Nullable;
import com.learn.spring.namespace.tag.AbstractBeanDefinitionParser;
import com.learn.spring.util.ClassUtils;
import com.learn.spring.util.StringUtils;
import org.w3c.dom.Element;

/**
 * Parser for the &lt;context:mbean-server/&gt; element.
 *
 * <p>Registers an instance of
 * {@link org.springframework.jmx.export.annotation.AnnotationMBeanExporter}
 * within the context.
 *
 * @author Mark Fisher
 * @author Juergen Hoeller
 * @since 2.5
 * @see org.springframework.jmx.export.annotation.AnnotationMBeanExporter
 */
public class MBeanServerBeanDefinitionParser extends AbstractBeanDefinitionParser {

	private static final String MBEAN_SERVER_BEAN_NAME = "mbeanServer";

	private static final String AGENT_ID_ATTRIBUTE = "agent-id";


	private static final boolean weblogicPresent;

	private static final boolean webspherePresent;

	static {
		ClassLoader classLoader = MBeanServerBeanDefinitionParser.class.getClassLoader();
		weblogicPresent = ClassUtils.isPresent("weblogic.management.Helper", classLoader);
		webspherePresent = ClassUtils.isPresent("com.ibm.websphere.management.AdminServiceFactory", classLoader);
	}


	@Override
	public String resolveId(Element element, AbstractBeanDefinition definition, ParserContext parserContext) {
		String id = element.getAttribute(ID_ATTRIBUTE);
		return (StringUtils.hasText(id) ? id : MBEAN_SERVER_BEAN_NAME);
	}

	@Override
	public AbstractBeanDefinition parseInternal(Element element, ParserContext parserContext) {
//
//		String agentId = element.getAttribute(AGENT_ID_ATTRIBUTE);
//		if (StringUtils.hasText(agentId)) {
//			RootBeanDefinition bd = new RootBeanDefinition(MBeanServerFactoryBean.class);
//			bd.getPropertyValues().add("agentId", agentId);
//			return bd;
//		}
//		AbstractBeanDefinition specialServer = findServerForSpecialEnvironment();
//		if (specialServer != null) {
//			return specialServer;
//		}
//		RootBeanDefinition bd = new RootBeanDefinition(MBeanServerFactoryBean.class);
//		bd.getPropertyValues().add("locateExistingServerIfPossible", Boolean.TRUE);
//
//		// Mark as infrastructure bean and attach source location.
//		bd.setRole(BeanDefinition.ROLE_INFRASTRUCTURE);
//		bd.setSource(parserContext.extractSource(element));
//		return bd;
		System.err.println("bolck 44");
		return (AbstractBeanDefinition) new Object();
	}

	@Nullable
	static AbstractBeanDefinition findServerForSpecialEnvironment() {
	    System.err.println("block 45");
//
		if (weblogicPresent) {
            System.err.println("block 453");
//			RootBeanDefinition bd = new RootBeanDefinition(JndiObjectFactoryBean.class);
//			bd.getPropertyValues().add("jndiName", "java:comp/env/jmx/runtime");
//			return bd;
		}
		else if (webspherePresent) {
            System.err.println("block 4555");

//			return new RootBeanDefinition(WebSphereMBeanServerFactoryBean.class);
		}
//		else {
//			return null;
//		}
		return null;
	}

}
