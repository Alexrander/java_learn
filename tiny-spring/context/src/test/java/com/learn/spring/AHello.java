package com.learn.spring;

import com.learn.spring.annotation.Bean;
import com.learn.spring.annotation.Component;

@Component
public class AHello {
    @Bean
    public String hello(){
        return "123321";
    }
}
