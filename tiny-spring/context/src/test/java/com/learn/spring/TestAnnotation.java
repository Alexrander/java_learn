package com.learn.spring;


import com.learn.spring.annotation.Component;
import com.learn.spring.annotation.ComponentScan;

import com.learn.spring.context.ApplicationContext;

@Component
@ComponentScan("com.learn.spring")
public class TestAnnotation {
    public static void main(String[] args){
//        ApplicationContext applicationContext = new App().createApplicationContext();
//        ((AnnotationConfigApplicationContext) context).refresh();
//        System.err.println(applicationContext.getBean("hello"));

    }
}
