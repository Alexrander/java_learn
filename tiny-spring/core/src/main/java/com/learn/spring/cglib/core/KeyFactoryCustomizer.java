package com.learn.spring.cglib.core;

/**
 * Marker interface for customizers of {@link KeyFactory}
 */
public interface KeyFactoryCustomizer {
}
