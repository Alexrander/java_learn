package com.learn.spring.env;

public interface EnvironmentCapable {

    Environment getEnvironment();

}
