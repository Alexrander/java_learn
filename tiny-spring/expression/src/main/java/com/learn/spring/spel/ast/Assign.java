/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.learn.spring.spel.ast;

import com.learn.spring.asm.Label;
import com.learn.spring.asm.MethodVisitor;
import com.learn.spring.lang.Nullable;
import com.learn.spring.sp.*;
import com.learn.spring.spel.CodeFlow;
import com.learn.spring.spel.ExpressionState;
import com.learn.spring.spel.SpelEvaluationException;
import com.learn.spring.spel.SpelMessage;
import com.learn.spring.spel.support.BooleanTypedValue;
import com.learn.spring.util.Assert;
import com.learn.spring.util.NumberUtils;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * Represents assignment. An alternative to calling setValue() for an expression is to use
 * an assign.
 *
 * <p>Example: 'someNumberProperty=42'
 *
 * @author Andy Clement
 * @since 3.0
 */
public class Assign extends SpelNodeImpl {

	public Assign(int pos,SpelNodeImpl... operands) {
		super(pos,operands);
	}


	@Override
	public TypedValue getValueInternal(ExpressionState state) throws EvaluationException {
		TypedValue newValue = this.children[1].getValueInternal(state);
		getChild(0).setValue(state, newValue.getValue());
		return newValue;
	}

	@Override
	public String toStringAST() {
		return getChild(0).toStringAST() + "=" + getChild(1).toStringAST();
	}

}
