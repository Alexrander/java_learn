package test;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class Main {

    @RequestMapping("/test")
    public String test(){
        return "123";
    }


    public static void main(String[] args){
        SpringApplication.run(Main.class,args);

    }
}
